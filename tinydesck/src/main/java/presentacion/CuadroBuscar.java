package presentacion;

import java.awt.AWTException;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Robot;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.event.InternalFrameAdapter;
import javax.swing.event.InternalFrameEvent;

import entidades.Busqueda;
import logicaNegocio.LogicaSistema;
import logicaNegocio.LogicaTickets;
import javax.swing.JPopupMenu;
import java.awt.Component;
import javax.swing.JMenuItem;

@SuppressWarnings("serial")
public class CuadroBuscar extends JInternalFrame {
	private JTextField tfCriterio;
	private JComboBox<String> cbLlave;
	private JTable tblResultado;

	private static CuadroBuscar instancia;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CuadroBuscar frame = new CuadroBuscar();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public static CuadroBuscar getInstancia() {
		if (instancia == null) {
			instancia = new CuadroBuscar();
		}
		return instancia;
	}

	/**
	 * Create the frame.
	 */
	private CuadroBuscar() {
		setName("CuadroBuscar");
		setClosable(true);
		addInternalFrameListener(new InternalFrameAdapter() {
			@Override
			public void internalFrameClosed(InternalFrameEvent e) {
				instancia = null;
			}
		});
		Font f = new Font("Segoe UI", Font.PLAIN, 14);
		setTitle("Cuadro de Busqueda");
		setBackground(Color.WHITE);
		setBounds(100, 100, 1024, 497);
		getContentPane().setLayout(null);

		JLabel lblImagen = new JLabel();
		lblImagen.setIcon(new ImageIcon(CuadroBuscar.class.getResource("/imagenes/tinydesck.png")));
		lblImagen.setBounds(6, 6, 168, 86);
		getContentPane().add(lblImagen);

		tfCriterio = new JTextField();
		tfCriterio.setFont(f);
		tfCriterio.setBounds(230, 52, 420, 26);
		getContentPane().add(tfCriterio);
		tfCriterio.setColumns(10);

		JLabel lblBuscar = new JLabel("Buscar");
		lblBuscar.setFont(f);
		lblBuscar.setBounds(184, 57, 50, 16);
		getContentPane().add(lblBuscar);

		JLabel lblPor = new JLabel("Por:");
		lblPor.setFont(f);
		lblPor.setBounds(660, 52, 33, 16);
		getContentPane().add(lblPor);

		cbLlave = new JComboBox<String>();
		cbLlave.setFont(f);
		cbLlave.setModel(new DefaultComboBoxModel<String>(Busqueda.getInstancia().getColumnas()));
		cbLlave.setBounds(696, 51, 197, 27);
		getContentPane().add(cbLlave);

		JButton btnBuscar = new JButton("Buscar");
		btnBuscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (validar()) {
					setCursor(new Cursor(Cursor.WAIT_CURSOR));
					Busqueda.getInstancia().setCriterioBusqueda(tfCriterio.getText());
					Busqueda.getInstancia().setLlaveBusqueda(cbLlave.getSelectedItem().toString());

					switch (Busqueda.getInstancia().getTipoBusqueda()) {
					case BUSCAR_DEPARTAMENTOS:

						break;
					case BUSCAR_TICKET_CLIENTE:
						tblResultado.setModel(LogicaTickets.dtmBuscarTicketsCliente());
						int[] tblTicket = { 23, 100, 124, 77, 71, 72, 93, 58 };
						for (int i = 0; i < tblResultado.getColumnModel().getColumnCount() - 1; i++) {
							tblResultado.getColumnModel().getColumn(i).setPreferredWidth(tblTicket[i]);
						}
						break;
					case BUSCAR_TICKET_TECNICO:
						tblResultado.setModel(LogicaTickets.dtmBuscarTicketsTecnico());
						int[] tblTecnico = { 26, 100, 136, 72, 74, 52, 99, 60, 80 };
						for (int i = 0; i < tblResultado.getColumnModel().getColumnCount() - 1; i++) {
							tblResultado.getColumnModel().getColumn(i).setPreferredWidth(tblTecnico[i]);
						}
						break;
					default:
						throw new IllegalArgumentException(
								"Unexpected value: " + Busqueda.getInstancia().getTipoBusqueda());
					}
					tblResultado.setRowHeight(30);
					setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
					JOptionPane.showMessageDialog(LogicaSistema.seleccionarDescktop(),
							"Se encontraron " + tblResultado.getModel().getRowCount() + " registros.",
							"Resultado busqueda", JOptionPane.INFORMATION_MESSAGE);

				} else {
					JOptionPane.showMessageDialog(LogicaSistema.seleccionarDescktop(),
							"Debe completar todos los criterios de busqueda", "Buscar", JOptionPane.WARNING_MESSAGE);
				}
			}
		});
		btnBuscar.setFont(f);
		btnBuscar.setBounds(903, 51, 99, 29);
		getContentPane().add(btnBuscar);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(6, 104, 996, 352);
		getContentPane().add(scrollPane);

		tblResultado = new JTable();
		tblResultado.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				boolean ejecutado = false;
				if ((e.getClickCount() == 2) && (!ejecutado)) {
					setCursor(new Cursor(Cursor.WAIT_CURSOR));
					ejecutado = true;
					disparaFrame();
					setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
				}
			}
		});

		tblResultado.setFont(f);
		tblResultado.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		scrollPane.setViewportView(tblResultado);

		JPopupMenu popupMenu = new JPopupMenu();
		addPopup(scrollPane, popupMenu);

		JMenuItem mntmNewMenuItem = new JMenuItem("Abrir Ticket");
		mntmNewMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				disparaFrame();
			}
		});
		mntmNewMenuItem.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		popupMenu.add(mntmNewMenuItem);

		JButton btnNewButton = new JButton("New button");
		btnNewButton.setVisible(false);
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tblResultado.getColumnModel().getColumnCount();

				for (int i = 0; i < tblResultado.getColumnModel().getColumnCount() - 1; i++) {
					System.out.print(tblResultado.getColumnModel().getColumn(i).getPreferredWidth() + ", ");
				}

			}
		});
		btnNewButton.setBounds(903, 6, 99, 35);
		getContentPane().add(btnNewButton);
	}

	protected void disparaFrame() {
		int numeroId = traerId();
		// System.out.println(numeroId);
		switch (Busqueda.getInstancia().getTipoBusqueda()) {
		case BUSCAR_DEPARTAMENTOS:

			break;
		case BUSCAR_TICKET_CLIENTE:
		case BUSCAR_TICKET_TECNICO:
			LogicaTickets.llamarTicket(numeroId);
			FrmTicket.getInstancia().toFront();
			break;
		default:
			throw new IllegalArgumentException("Unexpected value: " + Busqueda.getInstancia().getTipoBusqueda());
		}

	}

	private int traerId() {
		int row = tblResultado.getSelectedRow();
		return Integer.parseInt(tblResultado.getModel().getValueAt(row, 0).toString());
	}

	protected boolean validar() {
		if (tfCriterio.getText().equals("")) {
			return false;
		}

		if (cbLlave.getSelectedItem().toString().equals("")) {
			return false;
		}

		return true;
	}

	private static void addPopup(Component component, final JPopupMenu popup) {
		component.addMouseListener(new MouseAdapter() {

			@SuppressWarnings("deprecation")
			@Override
			public void mousePressed(MouseEvent e) {
				if (e.getButton() == 3) {
					try {
						Robot bot = new Robot();
						bot.mousePress(InputEvent.BUTTON1_MASK);
						bot.mouseRelease(InputEvent.BUTTON1_MASK);
					} catch (AWTException ex) {

					}
				}
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}

			private void showMenu(MouseEvent e) {
				popup.show(e.getComponent(), e.getX(), e.getY());
			}
		});
	}
}
