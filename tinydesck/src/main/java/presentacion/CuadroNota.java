package presentacion;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.border.LineBorder;
import javax.swing.event.InternalFrameAdapter;
import javax.swing.event.InternalFrameEvent;

import entidades.Nota;
import entidades.NotaEstado;
import entidades.TicketActivo;
import entidades.UsuarioCliente;
import logicaNegocio.LogicaSistema;
import logicaNegocio.LogicaTickets;

@SuppressWarnings("serial")
public class CuadroNota extends JInternalFrame {
	private static CuadroNota instancia;
	private JTextField tfAsunto;
	private JLabel lblEspacios;
	private JButton btnCrearNota;
	private JTextPane tfDetalle;
	private NotaEstado notaEstado;

	private Nota nota;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CuadroNota frame = new CuadroNota();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public static CuadroNota getInstancia() {
		if (instancia == null) {
			instancia = new CuadroNota();
		}
		return instancia;
	}

	/**
	 * Create the frame.
	 */
	private CuadroNota() {
		setName("CuadroNota");
		addInternalFrameListener(new InternalFrameAdapter() {
			@Override
			public void internalFrameClosed(InternalFrameEvent e) {
				instancia = null;
			}
		});
		Font f = new Font("Segoe UI", Font.PLAIN, 14);
		setClosable(true);
		getContentPane().setBackground(new Color(255, 255, 255));
		getContentPane().setLayout(null);

		tfAsunto = new JTextField("");
		tfAsunto.setFont(f);
		tfAsunto.setBounds(10, 39, 368, 32);
		getContentPane().add(tfAsunto);
		tfAsunto.setColumns(10);

		tfDetalle = new JTextPane();
		tfDetalle.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				int esp = 1024 - tfDetalle.getText().length();
				if (esp == 0) {
					lblEspacios.setText(esp + " caracteres de 1024");
				}

			}
		});
		tfDetalle.setFont(f);
		tfDetalle.setBorder(new LineBorder(new Color(0, 0, 0)));
		tfDetalle.setBounds(10, 108, 368, 189);
		getContentPane().add(tfDetalle);

		JLabel lblNewLabel = new JLabel("Asunto");
		lblNewLabel.setFont(f);
		lblNewLabel.setBounds(10, 10, 135, 19);
		getContentPane().add(lblNewLabel);

		JLabel lblDetalle = new JLabel("Detalle");
		lblDetalle.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		lblDetalle.setBounds(10, 79, 135, 19);
		getContentPane().add(lblDetalle);

		lblEspacios = new JLabel("0 caracteres de 1024");
		lblEspacios.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		lblEspacios.setBounds(10, 307, 230, 19);
		getContentPane().add(lblEspacios);

		btnCrearNota = new JButton("Crear Nota");
		btnCrearNota.setVisible(nota == null);
		btnCrearNota.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				nota = new Nota();
				if (notaEstado == null) {
					nota.setIdTicket(TicketActivo.getInstancia().getTicket().getIdTicket());
				} else {
					nota.setIdTicket(notaEstado.getIdTicket());
					if (notaEstado.getIdEstado() != 16) {
						LogicaTickets.cambiarEstadoNota(notaEstado);
						JOptionPane.showMessageDialog(LogicaSistema.seleccionarDescktop(),
								"Proceso realizado correctamente", "Cambio Estado Ticket", JOptionPane.INFORMATION_MESSAGE);
						CuadroBandejaEntrada.getInstancia().actualizarBandeja();
					}

				}

				nota.setAsunto(tfAsunto.getText());
				nota.setDetalleNota(tfDetalle.getText());
				nota.setIdUsuario(UsuarioCliente.getInstancia().getUsuario().getIdUsuario());
				LogicaTickets.crearNota(nota);

				dispose();
			}
		});
		btnCrearNota.setFont(f);
		btnCrearNota.setBounds(252, 303, 126, 26);
		getContentPane().add(btnCrearNota);

		setBounds(100, 100, 400, 373);
		setTitle("Cuadro de Notas");

	}

	public void cargarNota() {
		if (this.nota != null) {
			this.btnCrearNota.setVisible(false);
			this.tfAsunto.setText(this.nota.getAsunto());
			this.tfDetalle.setText(this.nota.getDetalleNota());
			this.lblEspacios.setText((1024 - this.nota.getDetalleNota().length()) + " caracteres de 2048");
		}
	}

	public Nota getNota() {
		return nota;
	}

	public void setNota(Nota nota) {
		this.nota = nota;
	}

	public NotaEstado getNotaEstado() {
		return notaEstado;
	}

	public void setNotaEstado(NotaEstado notaEstado) {
		this.notaEstado = notaEstado;
	}
}
