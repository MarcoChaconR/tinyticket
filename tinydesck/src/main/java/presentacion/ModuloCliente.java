package presentacion;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

import javax.swing.ImageIcon;
import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import entidades.Busqueda;
import entidades.Enumeradores.tipoBusqueda;
import hilos.HiloProductos;
import logicaNegocio.LogicaSistema;

@SuppressWarnings("serial")
public class ModuloCliente extends JFrame {
	public static JDesktopPane desktopPane = new JDesktopPane();
	private JPanel contentPane;
	private JLabel lblFondo;
	private JMenuBar menuBar;
	private JMenu mnTikets;
	private JMenu mnGeneral;
	private JMenuItem mnitmBuscarTikets;
	private JMenuItem mnitmNuevoTiket;
	private JMenuItem mnitmCambiarContrasena;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ModuloCliente frame = new ModuloCliente();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ModuloCliente() {
		HiloProductos hProductos = new HiloProductos();
		hProductos.start();

		setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		Font f = new Font("Segoe UI", Font.PLAIN, 14);
		setExtendedState(Frame.MAXIMIZED_BOTH);
		setFont(new Font("Segoe UI", Font.PLAIN, 13));
		setTitle("TinyDesck - Modulo Cliente - Sistema de Tickets");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 600);
		setLocationRelativeTo(null);

		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(0, 0, 0, 0));

		setContentPane(contentPane);

		contentPane.setLayout(new BorderLayout(0, 0));

		lblFondo = new JLabel();
		lblFondo.setBounds(0, 22, 790, 316);
		lblFondo.setIcon(new ImageIcon(ModuloCliente.class.getResource("/imagenes/fondo.jpg")));

		desktopPane.addComponentListener(new ComponentAdapter() {
			@Override
			public void componentResized(ComponentEvent arg0) {
				lblFondo.setLocation(((int) (desktopPane.getWidth() - lblFondo.getWidth()) / 2),
						((int) (desktopPane.getHeight() - lblFondo.getHeight()) / 2));
				menuBar.setBounds(0, 0, getWidth(), 22);
			}
		});

		desktopPane.setBackground(Color.WHITE);
		contentPane.add(desktopPane);

		desktopPane.add(lblFondo);

		menuBar = new JMenuBar();
		menuBar.setBounds(0, 0, this.getWidth(), 22);
		desktopPane.add(menuBar);

		mnGeneral = new JMenu("General");
		mnGeneral.setFont(f);
		menuBar.add(mnGeneral);

		mnitmCambiarContrasena = new JMenuItem("Cambiar Contraseña");
		mnitmCambiarContrasena.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String nuevaContrasema = JOptionPane.showInputDialog("Indique la nueva Contraseña");
				if (nuevaContrasema != null) {
					LogicaSistema.cambiarContraseña(nuevaContrasema);
				} else {
					JOptionPane.showMessageDialog(null, "No se realizará el cambio", "Sin Contraseña",
							JOptionPane.WARNING_MESSAGE);
				}
			}
		});
		mnitmCambiarContrasena.setFont(f);
		mnGeneral.add(mnitmCambiarContrasena);

		mnTikets = new JMenu("Tikets");
		mnTikets.setFont(f);
		menuBar.add(mnTikets);

		mnitmBuscarTikets = new JMenuItem("Consultar Tikets");
		mnitmBuscarTikets.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Busqueda.getInstancia()
						.setColumnas(new String[] { "", "NumeroCaso", "Estado", "Prioridad", "Servicio", "Categoria" });
				Busqueda.getInstancia().setTipoBusqueda(tipoBusqueda.BUSCAR_TICKET_CLIENTE);

				CuadroBuscar.getInstancia().setVisible(true);

				desktopPane.add(CuadroBuscar.getInstancia());
				CuadroBuscar.getInstancia().setLocation(p(CuadroBuscar.getInstancia()));
				CuadroBuscar.getInstancia().toFront();
			}
		});
		mnitmBuscarTikets.setFont(f);
		mnTikets.add(mnitmBuscarTikets);

		mnitmNuevoTiket = new JMenuItem("Crear Nuevo Tiket");
		mnitmNuevoTiket.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				FrmTicket.getInstancia().setVisible(true);
				desktopPane.add(FrmTicket.getInstancia());
				FrmTicket.getInstancia().setLocation(p(FrmTicket.getInstancia()));
				FrmTicket.getInstancia().toFront();
			}
		});
		mnitmNuevoTiket.setFont(f);
		mnTikets.add(mnitmNuevoTiket);
	}

	private Point p(JInternalFrame frame) {
		return new Point(((int) (desktopPane.getWidth() - frame.getWidth()) / 2),
				((int) (desktopPane.getHeight() - frame.getHeight()) / 2));
	}
}
