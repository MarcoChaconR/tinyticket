package presentacion;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JInternalFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import logicaNegocio.LogicaTickets;
import java.awt.Color;

@SuppressWarnings("serial")
public class CuadroBitacora extends JInternalFrame {
	private JTable tbBitacora;
	private static CuadroBitacora instancia;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CuadroBitacora frame = new CuadroBitacora();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public static CuadroBitacora getInstancia() {
		if (instancia == null) {
			instancia = new CuadroBitacora();
		}
		return instancia;
	}

	/**
	 * Create the frame.
	 */
	private CuadroBitacora() {
		getContentPane().setBackground(Color.WHITE);
		setBackground(Color.WHITE);
		setName("CuadroBitacora");
		setClosable(true);
		setTitle("Cuadro Bitacora de Modificaciones");
		setBounds(100, 100, 600, 300);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBackground(Color.WHITE);
		getContentPane().add(scrollPane, BorderLayout.CENTER);

		tbBitacora = new JTable();
		scrollPane.setViewportView(tbBitacora);

	}

	public void llenarBitacora() {
		tbBitacora.setModel(LogicaTickets.dtmBitacora());
		tbBitacora.setRowHeight(25);
	}

}
