package presentacion;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

import javax.swing.ImageIcon;
import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import entidades.Busqueda;
import entidades.Enumeradores.tipoBusqueda;
import hilos.HiloProductos;
import logicaNegocio.LogicaSistema;
import logicaNegocio.LogicaTickets;

@SuppressWarnings("serial")
public class ModuloAdministrador extends JFrame {
	public static JDesktopPane desktopPane = new JDesktopPane();
	private JPanel contentPane;
	private JLabel lblFondo;
	private JMenuBar menuBar;
	private JMenu mnNewMenu;
	private JMenuItem mntmNewMenuItem;
	private JMenu mnNewMenu_1;
	private JMenuItem mntmNewMenuItem_1;
	private JMenuItem mntmNewMenuItem_2;
	private JMenu mnNewMenu_2;
	private JMenu mnNewMenu_3;
	private JMenu mnNewMenu_4;
	private JMenuItem mntmNewMenuItem_3;
	private JMenuItem mntmNewMenuItem_4;
	private JMenuItem mntmNewMenuItem_5;
	private JMenuItem mntmNewMenuItem_6;
	private JMenuItem mntmNewMenuItem_7;
	private JMenuItem mntmNewMenuItem_15;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ModuloAdministrador frame = new ModuloAdministrador();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ModuloAdministrador() {
		HiloProductos hProductos = new HiloProductos();
		hProductos.start();

		setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		Font f = new Font("Segoe UI", Font.PLAIN, 14);
		setExtendedState(Frame.MAXIMIZED_BOTH);
		setFont(f);
		setTitle("TinyDesck - Modulo Usuario Administrador - Sistema de Tickets");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 802, 610);
		setLocationRelativeTo(null);

		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(0, 0, 0, 0));

		setContentPane(contentPane);

		contentPane.setLayout(new BorderLayout(0, 0));

		lblFondo = new JLabel();
		lblFondo.setBounds(0, 22, 626, 326);
		lblFondo.setIcon(new ImageIcon(ModuloCliente.class.getResource("/imagenes/fondoAdministrador.png")));

		desktopPane.addComponentListener(new ComponentAdapter() {
			@Override
			public void componentResized(ComponentEvent arg0) {
				lblFondo.setLocation(((int) (desktopPane.getWidth() - lblFondo.getWidth()) / 2),
						((int) (desktopPane.getHeight() - lblFondo.getHeight()) / 2));
				menuBar.setBounds(0, 0, getWidth(), 22);
			}
		});

		desktopPane.setBackground(Color.WHITE);
		contentPane.add(desktopPane);

		desktopPane.add(lblFondo);

		menuBar = new JMenuBar();
		menuBar.setFont(f);
		menuBar.setBounds(0, 0, this.getWidth(), 22);
		desktopPane.add(menuBar);

		mnNewMenu = new JMenu("General");
		mnNewMenu.setFont(f);
		menuBar.add(mnNewMenu);

		mntmNewMenuItem = new JMenuItem("Cambiar Contraseña");
		mntmNewMenuItem.setFont(f);
		mntmNewMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String nuevaContrasema = JOptionPane.showInputDialog("Indique la nueva Contraseña");
				if (nuevaContrasema != null) {
					LogicaSistema.cambiarContraseña(nuevaContrasema);
				} else {
					JOptionPane.showMessageDialog(LogicaSistema.seleccionarDescktop(), "No se realizará el cambio",
							"Sin Contraseña", JOptionPane.WARNING_MESSAGE);
				}
			}
		});
		mnNewMenu.add(mntmNewMenuItem);

		mnNewMenu_1 = new JMenu("Gestion de Usuarios");
		mnNewMenu_1.setFont(f);
		menuBar.add(mnNewMenu_1);

		mntmNewMenuItem_1 = new JMenuItem("Desbloquear Usuario");
		mntmNewMenuItem_1.setFont(f);
		mnNewMenu_1.add(mntmNewMenuItem_1);

		mntmNewMenuItem_2 = new JMenuItem("Cambiar Contraseña a Usuario");
		mntmNewMenuItem_2.setFont(f);
		mnNewMenu_1.add(mntmNewMenuItem_2);

		JMenu mnNewMenu_6 = new JMenu("Gestion Sistema");
		mnNewMenu_6.setFont(f);
		menuBar.add(mnNewMenu_6);

		JMenu mntmNewMenuItem_9 = new JMenu("Mantenimiento Productos");
		mntmNewMenuItem_9.setFont(f);
		mnNewMenu_6.add(mntmNewMenuItem_9);

		JMenuItem mntmNewMenuItem_14 = new JMenuItem("Agregar Producto");
		mntmNewMenuItem_14.setFont(f);
		mntmNewMenuItem_14.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				LogicaSistema.agregarProducto();
			}
		});
		mntmNewMenuItem_9.add(mntmNewMenuItem_14);

		mntmNewMenuItem_15 = new JMenuItem("Eliminar Producto");
		mntmNewMenuItem_15.setFont(f);
		mntmNewMenuItem_15.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				LogicaSistema.eliminarProductos();
			}
		});
		mntmNewMenuItem_9.add(mntmNewMenuItem_15);

		JMenuItem mntmNewMenuItem_10 = new JMenuItem("Mantenimiento de Estados");
		mntmNewMenuItem_10.setFont(f);
		mnNewMenu_6.add(mntmNewMenuItem_10);

		JMenuItem mntmNewMenuItem_11 = new JMenuItem("Mantenimiento Departamentos");
		mntmNewMenuItem_11.setFont(f);
		mnNewMenu_6.add(mntmNewMenuItem_11);

		mnNewMenu_2 = new JMenu("Informes");
		mnNewMenu_2.setFont(f);
		menuBar.add(mnNewMenu_2);

		mnNewMenu_3 = new JMenu("Formato XLS");
		mnNewMenu_3.setFont(f);
		mnNewMenu_2.add(mnNewMenu_3);

		mntmNewMenuItem_3 = new JMenuItem("Registros Tickets");
		mntmNewMenuItem_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				LogicaTickets.exportarXLS("SELECT * FROM u902958937_TikectDesk.vTickets");
			}
		});
		mntmNewMenuItem_3.setFont(f);
		mnNewMenu_3.add(mntmNewMenuItem_3);

		mntmNewMenuItem_4 = new JMenuItem("Reporte de Usuarios");
		mntmNewMenuItem_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				LogicaTickets.exportarXLS("SELECT * FROM u902958937_TikectDesk.vUsuarios");
			}
		});
		mntmNewMenuItem_4.setFont(f);
		mnNewMenu_3.add(mntmNewMenuItem_4);

		JMenuItem mntmNewMenuItem_12 = new JMenuItem("Reporte de Gestiones");
		mntmNewMenuItem_12.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				LogicaTickets.exportarXLS("SELECT * FROM u902958937_TikectDesk.tbl_Notas");
			}
		});
		mntmNewMenuItem_12.setFont(f);
		mnNewMenu_3.add(mntmNewMenuItem_12);

		JMenuItem mntmNewMenuItem_13 = new JMenuItem("Satisfaccion Clientes");
		mntmNewMenuItem_13.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				LogicaTickets.exportarXLS("SELECT * FROM u902958937_TikectDesk.vCalificaciones");
			}
		});
		mntmNewMenuItem_13.setFont(f);
		mnNewMenu_3.add(mntmNewMenuItem_13);

		mnNewMenu_4 = new JMenu("Formato PDF");
		mnNewMenu_4.setFont(f);
		mnNewMenu_2.add(mnNewMenu_4);

		mntmNewMenuItem_5 = new JMenuItem("Registros Tickets General");
		mntmNewMenuItem_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

			}
		});
		mntmNewMenuItem_5.setFont(f);
		mnNewMenu_4.add(mntmNewMenuItem_5);

		mntmNewMenuItem_6 = new JMenuItem("Registro Tickets por Usuario");
		mntmNewMenuItem_6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

			}
		});
		mntmNewMenuItem_6.setFont(f);
		mnNewMenu_4.add(mntmNewMenuItem_6);

		mntmNewMenuItem_7 = new JMenuItem("Resumen General");
		mntmNewMenuItem_7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

			}
		});
		mntmNewMenuItem_7.setFont(f);
		mnNewMenu_4.add(mntmNewMenuItem_7);

		JMenu mnNewMenu_5 = new JMenu("Tickets");
		mnNewMenu_5.setFont(f);
		menuBar.add(mnNewMenu_5);

		JMenuItem mntmNewMenuItem_8 = new JMenuItem("Buscar Tickets");
		mntmNewMenuItem_8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Busqueda.getInstancia()
						.setColumnas(new String[] { "", "NumeroCaso", "Estado", "Prioridad", "UsuarioCreador" });
				Busqueda.getInstancia().setTipoBusqueda(tipoBusqueda.BUSCAR_TICKET_CLIENTE);

				CuadroBuscar.getInstancia().setVisible(true);

				desktopPane.add(CuadroBuscar.getInstancia());
				CuadroBuscar.getInstancia().setLocation(p(CuadroBuscar.getInstancia()));
				CuadroBuscar.getInstancia().toFront();
			}
		});
		mntmNewMenuItem_8.setFont(f);
		mnNewMenu_5.add(mntmNewMenuItem_8);
	}

	private Point p(JInternalFrame frame) {
		return new Point(((int) (desktopPane.getWidth() - frame.getWidth()) / 2),
				((int) (desktopPane.getHeight() - frame.getHeight()) / 2));
	}
}
