package presentacion;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import entidades.TicketActivo;
import hilos.HiloCorreo;
import logicaNegocio.LogicaUsuarios;
import java.awt.event.MouseMotionAdapter;
import java.awt.Dimension;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

public class CuadroLogin {

	private JFrame frame;
	private JTextField tfUsuario;
	private JPasswordField tfContrasena;
	private int xx;
	private int xy;
	private int intentos;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CuadroLogin window = new CuadroLogin();
					window.frame.setVisible(true);
					window.frame.setLocationRelativeTo(null);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public CuadroLogin() {
		intentos = 0;
		Font f = new Font("Segoe UI", Font.PLAIN, 18);
		frame = new JFrame();
		frame.addComponentListener(new ComponentAdapter() {
			@Override
			public void componentResized(ComponentEvent e) {
				frame.setSize(new Dimension(800, 600));
			}
		});
		frame.setSize(new Dimension(800, 600));
		frame.setName("frmLogin");
		frame.setMinimumSize(new Dimension(800, 600));
		frame.setMaximumSize(new Dimension(800, 600));
		frame.getContentPane().addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseDragged(MouseEvent e) {
				int x = e.getXOnScreen();
				int y = e.getYOnScreen();
				moverFrame(x, y);
			}
		});
		frame.getContentPane().addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				xx = e.getX();
				xy = e.getY();
			}
		});

		frame.setUndecorated(true);
		frame.getContentPane().setBackground(new Color(255, 255, 255));
		frame.getContentPane().setLayout(null);

		JLabel lblTinyTicket = new JLabel("TinyTicket");
		lblTinyTicket.setHorizontalAlignment(SwingConstants.RIGHT);
		lblTinyTicket.setForeground(Color.WHITE);
		lblTinyTicket.setFont(new Font("Tahoma", Font.PLAIN, 36));
		lblTinyTicket.setBounds(0, 502, 286, 88);
		frame.getContentPane().add(lblTinyTicket);

		JLabel lblPanel = new JLabel("");
		lblPanel.setIcon(new ImageIcon(CuadroLogin.class.getResource("/imagenes/bacground.jpg")));
		lblPanel.setBounds(0, 0, 300, 600);
		frame.getContentPane().add(lblPanel);

		JLabel lblBienvenido = new JLabel("Bienvenido al Sistema");
		lblBienvenido.setFont(new Font("Lucida Grande", Font.BOLD, 30));
		lblBienvenido.setBounds(372, 75, 355, 38);
		frame.getContentPane().add(lblBienvenido);

		JLabel lblCerrar = new JLabel("X");
		lblCerrar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				System.exit(0);
			}
		});
		lblCerrar.setFont(new Font("Arial", Font.BOLD, 25));
		lblCerrar.setBounds(777, 6, 17, 27);
		frame.getContentPane().add(lblCerrar);

		tfUsuario = new JTextField();
		tfUsuario.setHorizontalAlignment(SwingConstants.CENTER);
		tfUsuario.setFont(new Font("Lucida Grande", Font.PLAIN, 22));
		tfUsuario.setBounds(372, 222, 355, 38);
		frame.getContentPane().add(tfUsuario);
		tfUsuario.setColumns(10);

		JLabel lblUsuario = new JLabel("Usuario");
		lblUsuario.setFont(f);
		lblUsuario.setBounds(372, 183, 117, 27);
		frame.getContentPane().add(lblUsuario);

		JLabel lblContrasea = new JLabel("Contraseña");
		lblContrasea.setFont(f);
		lblContrasea.setBounds(372, 301, 192, 27);
		frame.getContentPane().add(lblContrasea);

		tfContrasena = new JPasswordField();
		tfContrasena.setHorizontalAlignment(SwingConstants.CENTER);
		tfContrasena.setFont(new Font("Lucida Grande", Font.PLAIN, 22));
		tfContrasena.setColumns(10);
		tfContrasena.setBounds(372, 338, 355, 38);
		frame.getContentPane().add(tfContrasena);

		JButton btnNewButton = new JButton("Ingresar");
		btnNewButton.setFont(f);
		btnNewButton.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				intentos++;
				if ((!tfUsuario.getText().equals("")) && (!String.valueOf(tfContrasena.getPassword()).equals(""))) {
					frame.setCursor(new Cursor(Cursor.WAIT_CURSOR));
					if (LogicaUsuarios.validarUsuario(tfUsuario.getText(), charString(tfContrasena.getPassword()),
							intentos)) {
						frame.setVisible(false);
					}
				} else {
					JOptionPane.showMessageDialog(frame, "Debe ingresar todos los datos", "Error de inicio",
							JOptionPane.WARNING_MESSAGE);
				}
				frame.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
			}
		});
		btnNewButton.setBounds(496, 428, 117, 38);
		frame.getContentPane().add(btnNewButton);

		JButton Registrarse = new JButton("Registrarse");
		Registrarse.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (((!tfUsuario.getText().equals("")) && (!String.valueOf(tfContrasena.getPassword()).equals("")))
						&& (validarUsuarioCorreo(tfUsuario.getText()))
						&& (validarContrasena(String.valueOf(tfContrasena.getPassword())))
						&& (LogicaUsuarios.usuarioSinRegistrar(tfUsuario.getText()))) {
					LogicaUsuarios.registrarNuevoUsuario(tfUsuario.getText(),
							String.valueOf(tfContrasena.getPassword()));

					tfContrasena.setText("");
					tfUsuario.setText("");

				} else if (!validarUsuarioCorreo(tfUsuario.getText())) {
					JOptionPane.showMessageDialog(null, "El nombre de usuario debe ser un correo electrónico valido",
							"Error de Registro", JOptionPane.ERROR_MESSAGE);
				} else if (!validarContrasena(String.valueOf(tfContrasena.getPassword()))) {
					JOptionPane.showMessageDialog(null, "La contraseña debe tener al menos un numero, al menos "
							+ "una mayuscula, al menos una minuscula, debe tener al menos 8 caracteres y como maximo 20 caracteres",
							"Error de Registro", JOptionPane.ERROR_MESSAGE);
				} else if (!LogicaUsuarios.usuarioSinRegistrar(tfUsuario.getText())) {
					JOptionPane.showMessageDialog(null, "El Usuario ya se encuentra registrado", "Error de Registro",
							JOptionPane.ERROR_MESSAGE);
				} else {
					JOptionPane.showMessageDialog(null, "Debe llenar todos los campos", "Error de Registro",
							JOptionPane.ERROR_MESSAGE);
				}
			}

		});
		Registrarse.setForeground(new Color(0, 0, 128));
		Registrarse.setFont(new Font("Tahoma", Font.PLAIN, 13));
		Registrarse.setBackground(new Color(255, 255, 255));
		Registrarse.setBorder(null);
		Registrarse.setBounds(496, 502, 117, 27);
		frame.getContentPane().add(Registrarse);
		frame.setBounds(100, 100, 800, 600);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setResizable(false);

	}

	protected boolean validarContrasena(String contrasena) {
		String expresionRegular = "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{8,20}$";
		return contrasena.matches(expresionRegular);
	}

	private boolean validarUsuarioCorreo(String correoElectronico) {
		String expresionRegular = "^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,}$";
		return correoElectronico.matches(expresionRegular);
	}

	private String charString(char[] valor) {
		String hilera = "";
		for (char c : valor) {
			hilera += c;
		}
		return hilera;
	}

	private void moverFrame(int x, int y) {
		frame.setLocation(x - xx, y - xy);
	}
}
