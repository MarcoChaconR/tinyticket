package presentacion;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class StarRatingPanel extends JPanel {
	private int rating;
	private JButton[] starButtons;

	public StarRatingPanel() {
		rating = 0;
		starButtons = new JButton[5];

		setLayout(new FlowLayout());

		for (int i = 0; i < 5; i++) {
			JButton starButton = new JButton(
					new ImageIcon(StarRatingPanel.class.getResource("/imagenes/star-empty.png")));
			starButton.setBorderPainted(false);
			starButton.setContentAreaFilled(false);
			starButton.setOpaque(false);
			starButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					setRating(getSelectedIndex(e.getSource()) + 1);
				}
			});
			add(starButton);
			starButtons[i] = starButton;
		}
	}

	private int getSelectedIndex(Object source) {
		for (int i = 0; i < starButtons.length; i++) {
			if (source == starButtons[i]) {
				return i;
			}
		}
		return -1;
	}

	public void setRating(int rating) {
		this.rating = rating;
		for (int i = 0; i < starButtons.length; i++) {
			if (i < this.rating) {
				starButtons[i].setIcon(new ImageIcon(StarRatingPanel.class.getResource("/imagenes/star-filled.png")));
			} else {
				starButtons[i].setIcon(new ImageIcon(StarRatingPanel.class.getResource("/imagenes/star-empty.png")));
			}
		}
	}

	public int getRating() {
		return rating;
	}

}
