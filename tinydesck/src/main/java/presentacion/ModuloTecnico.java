package presentacion;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

import javax.swing.ImageIcon;
import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import entidades.Busqueda;
import entidades.Enumeradores.tipoBusqueda;
import hilos.HiloProductos;
import logicaNegocio.LogicaSistema;

@SuppressWarnings("serial")
public class ModuloTecnico extends JFrame {
	public static JDesktopPane desktopPane = new JDesktopPane();
	private JPanel contentPane;
	private JLabel lblFondo;
	private JMenuBar menuBar;
	private JMenu mnNewMenu;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ModuloTecnico frame = new ModuloTecnico();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ModuloTecnico() {
		HiloProductos hProductos = new HiloProductos();
		hProductos.start();
		
		setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		Font f = new Font("Segoe UI", Font.PLAIN, 14);
		setExtendedState(Frame.MAXIMIZED_BOTH);
		setFont(f);
		setTitle("TinyDesck - Modulo Usuario Técnico - Sistema de Tickets");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 802, 610);
		setLocationRelativeTo(null);

		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(0, 0, 0, 0));

		setContentPane(contentPane);

		contentPane.setLayout(new BorderLayout(0, 0));

		lblFondo = new JLabel();
		lblFondo.setBounds(0, 66, 800, 365);
		lblFondo.setIcon(new ImageIcon(ModuloCliente.class.getResource("/imagenes/fondoTecnico.jpg")));

		desktopPane.addComponentListener(new ComponentAdapter() {
			@Override
			public void componentResized(ComponentEvent arg0) {
				lblFondo.setLocation(((int) (desktopPane.getWidth() - lblFondo.getWidth()) / 2),
						((int) (desktopPane.getHeight() - lblFondo.getHeight()) / 2));
				menuBar.setBounds(0, 0, getWidth(), 22);
			}
		});

		desktopPane.setBackground(Color.WHITE);
		contentPane.add(desktopPane);

		menuBar = new JMenuBar();
		menuBar.setFont(f);
		menuBar.setBounds(0, 0, this.getWidth(), 22);
		desktopPane.add(menuBar);

		mnNewMenu = new JMenu("General");
		mnNewMenu.setFont(f);
		menuBar.add(mnNewMenu);

		JMenuItem mntmNewMenuItem = new JMenuItem("Cambiar Contraseña");
		mntmNewMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String nuevaContrasema = JOptionPane.showInputDialog("Indique la nueva Contraseña");
				if (nuevaContrasema != null) {
					LogicaSistema.cambiarContraseña(nuevaContrasema);
				} else {
					JOptionPane.showMessageDialog(null, "No se realizará el cambio", "Sin Contraseña",
							JOptionPane.WARNING_MESSAGE);
				}
			}
		});
		mntmNewMenuItem.setFont(f);
		mnNewMenu.add(mntmNewMenuItem);

		JMenu mnNewMenu_1 = new JMenu("Tickets");
		mnNewMenu_1.setFont(f);
		menuBar.add(mnNewMenu_1);

		JMenuItem mntmNewMenuItem_1 = new JMenuItem("Abrir Bandeja de Entrada");
		mntmNewMenuItem_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CuadroBandejaEntrada.getInstancia().setVisible(true);
				if (!LogicaSistema.estaAbierto(desktopPane, "CuadroBandejaEntrada")) {
					desktopPane.add(CuadroBandejaEntrada.getInstancia());
				}
				CuadroBandejaEntrada.getInstancia().actualizarBandeja();
				CuadroBandejaEntrada.getInstancia().setLocation(p(CuadroBandejaEntrada.getInstancia()));
				CuadroBandejaEntrada.getInstancia().toFront();

			}
		});
		mntmNewMenuItem_1.setFont(f);
		mnNewMenu_1.add(mntmNewMenuItem_1);

		JMenuItem mntmNewMenuItem_2 = new JMenuItem("Buscar Ticket");
		mntmNewMenuItem_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Busqueda.getInstancia().setColumnas(new String[] { "", "NumeroCaso", "Estado", "Prioridad", "Servicio",
						"Categoria", "NombreUsuario" });
				Busqueda.getInstancia().setTipoBusqueda(tipoBusqueda.BUSCAR_TICKET_TECNICO);

				CuadroBuscar.getInstancia().setVisible(true);
				if (!LogicaSistema.estaAbierto(desktopPane, "CuadroBuscar")) {
					desktopPane.add(CuadroBuscar.getInstancia());
				}

				CuadroBuscar.getInstancia().setLocation(p(CuadroBuscar.getInstancia()));
				CuadroBuscar.getInstancia().toFront();
			}
		});
		mntmNewMenuItem_2.setFont(f);
		mnNewMenu_1.add(mntmNewMenuItem_2);

		desktopPane.add(lblFondo);
	}

	private Point p(JInternalFrame frame) {
		return new Point(((int) (desktopPane.getWidth() - frame.getWidth()) / 2),
				((int) (desktopPane.getHeight() - frame.getHeight()) / 2));
	}
}
