package presentacion;

import java.awt.AWTException;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Robot;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.Vector;

import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.event.InternalFrameAdapter;
import javax.swing.event.InternalFrameEvent;

import entidades.Nota;
import entidades.NotaEstado;
import entidades.Producto;
import entidades.Ticket;
import entidades.TicketActivo;
import entidades.Usuario;
import entidades.UsuarioCliente;
import hilos.HiloCorreo;
import hilos.HiloProductos;
import logicaNegocio.LogicaSistema;
import logicaNegocio.LogicaTickets;

@SuppressWarnings("serial")
public class FrmTicket extends JInternalFrame {
	public JTable tblNotas;
	private JTextField tfNumeroTicket;
	private JTextField tfUsuarioCreador;
	private JTextField tfUsuarioAsignado;
	private JTextField tfFechaSolicitud;
	private JTextField tfFechaCierre;
	private JComboBox<String> cbTipoProducto;
	private JComboBox<String> cbNombreProducto;
	private JComboBox<String> cbPrioridad;
	private JButton btnRegistrar;
	private JLabel lblEstado;
	private JTextPane tpDetalle;
	private JMenuItem miReasignar;
	private JMenuItem miCalificacion;
	private JMenuItem miEscalar;
	private JMenuItem miCerrarCaso;
	private JMenuItem miAtendido;
	private static FrmTicket instancia;
	private Ticket ticket;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmTicket frame = new FrmTicket();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public static FrmTicket getInstancia() {
		if (instancia == null) {
			instancia = new FrmTicket();
		}

		return instancia;
	}

	/**
	 * Create the frame.
	 */
	private FrmTicket() {
		setClosable(true);
		setName("FrmTicket");
		addInternalFrameListener(new InternalFrameAdapter() {
			@Override
			public void internalFrameClosed(InternalFrameEvent e) {
				instancia = null;
			}
		});

		setBackground(Color.WHITE);
		Font f = new Font("Segoe UI", Font.PLAIN, 14);
		setTitle("Registro de Tickets");
		setBounds(100, 100, 847, 472);
		getContentPane().setLayout(null);

		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon(FrmTicket.class.getResource("/imagenes/tinydesck.png")));
		lblNewLabel.setBounds(6, 25, 168, 86);
		getContentPane().add(lblNewLabel);

		JPanel pnlEstado = new JPanel();
		pnlEstado.setBackground(new Color(65, 103, 169));
		pnlEstado.setBounds(447, 33, 353, 67);
		getContentPane().add(pnlEstado);
		pnlEstado.setLayout(null);

		lblEstado = new JLabel("Nuevo Caso");
		lblEstado.setFont(new Font("Lucida Grande", Font.BOLD, 22));
		lblEstado.setHorizontalAlignment(SwingConstants.CENTER);
		lblEstado.setForeground(Color.WHITE);
		lblEstado.setBounds(6, 6, 341, 53);
		pnlEstado.add(lblEstado);

		JTabbedPane tbbpTicket = new JTabbedPane(JTabbedPane.TOP);
		tbbpTicket.setFont(new Font("Tahoma", Font.PLAIN, 14));
		tbbpTicket.setBounds(6, 123, 811, 299);
		getContentPane().add(tbbpTicket);

		JPanel General = new JPanel();
		General.setBackground(Color.WHITE);
		General.setLayout(null);
		tbbpTicket.addTab("Datos Generales", null, General, null);

		JLabel lblTN = new JLabel("Numero Ticket");
		lblTN.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		lblTN.setBounds(10, 10, 103, 20);
		General.add(lblTN);

		JLabel lblCategoria = new JLabel("Categoria");
		lblCategoria.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		lblCategoria.setBounds(10, 88, 103, 20);
		General.add(lblCategoria);

		cbTipoProducto = new JComboBox<String>();
		cbTipoProducto.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				cbNombreProducto.setModel(dcbModelProductos());
			}
		});
		cbTipoProducto.setModel(dcbModelCateforias());
		cbTipoProducto.setFont(f);
		cbTipoProducto.setBounds(10, 118, 159, 28);
		General.add(cbTipoProducto);

		tfNumeroTicket = new JTextField();
		tfNumeroTicket.setEditable(false);
		tfNumeroTicket.setHorizontalAlignment(SwingConstants.CENTER);
		tfNumeroTicket.setFont(f);
		tfNumeroTicket.setBounds(10, 40, 159, 28);
		General.add(tfNumeroTicket);
		tfNumeroTicket.setColumns(10);

		cbNombreProducto = new JComboBox<String>();
		cbNombreProducto.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		cbNombreProducto.setBounds(179, 118, 294, 28);
		General.add(cbNombreProducto);

		JLabel lblServicio = new JLabel("Tipo de Servicio");
		lblServicio.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		lblServicio.setBounds(179, 88, 103, 20);
		General.add(lblServicio);

		JLabel lblUsuarioCliente = new JLabel("Autor");
		lblUsuarioCliente.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		lblUsuarioCliente.setBounds(179, 10, 103, 20);
		General.add(lblUsuarioCliente);

		tfUsuarioCreador = new JTextField();
		tfUsuarioCreador.setHorizontalAlignment(SwingConstants.CENTER);
		tfUsuarioCreador.setEditable(false);
		tfUsuarioCreador.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		tfUsuarioCreador.setColumns(10);
		tfUsuarioCreador.setBounds(179, 40, 190, 28);
		tfUsuarioCreador.setText(UsuarioCliente.getInstancia().getUsuario().getNombreUsuario());
		General.add(tfUsuarioCreador);

		JLabel lblAsignado = new JLabel("Tecnico Asignado");
		lblAsignado.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		lblAsignado.setBounds(379, 10, 112, 20);
		General.add(lblAsignado);

		tfUsuarioAsignado = new JTextField();
		tfUsuarioAsignado.setHorizontalAlignment(SwingConstants.CENTER);
		tfUsuarioAsignado.setEditable(false);
		tfUsuarioAsignado.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		tfUsuarioAsignado.setColumns(10);
		tfUsuarioAsignado.setBounds(379, 40, 190, 28);
		General.add(tfUsuarioAsignado);

		JLabel lblFechaSolicitud = new JLabel("Fecha Solicitud");
		lblFechaSolicitud.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		lblFechaSolicitud.setBounds(579, 10, 112, 20);
		General.add(lblFechaSolicitud);

		tfFechaSolicitud = new JTextField();
		tfFechaSolicitud.setEditable(false);
		tfFechaSolicitud.setHorizontalAlignment(SwingConstants.CENTER);
		tfFechaSolicitud.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		tfFechaSolicitud.setColumns(10);
		tfFechaSolicitud.setBounds(579, 40, 190, 28);
		tfFechaSolicitud.setText(new SimpleDateFormat("dd/MM/yyyy hh:mm").format(new Date()));
		General.add(tfFechaSolicitud);

		JLabel lblFechaCierre = new JLabel("Fecha Cierre");
		lblFechaCierre.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		lblFechaCierre.setBounds(10, 164, 112, 20);
		General.add(lblFechaCierre);

		tfFechaCierre = new JTextField();
		tfFechaCierre.setEditable(false);
		tfFechaCierre.setHorizontalAlignment(SwingConstants.CENTER);
		tfFechaCierre.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		tfFechaCierre.setColumns(10);
		tfFechaCierre.setBounds(10, 194, 190, 28);
		General.add(tfFechaCierre);

		JLabel lblPrioridad = new JLabel("Prioridad");
		lblPrioridad.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		lblPrioridad.setBounds(483, 88, 103, 20);
		General.add(lblPrioridad);

		cbPrioridad = new JComboBox<String>();
		cbPrioridad.setModel(new DefaultComboBoxModel<String>(new String[] { "Baja", "Media", "Alta" }));
		cbPrioridad.setSelectedIndex(1);
		cbPrioridad.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		cbPrioridad.setBounds(483, 118, 159, 28);
		General.add(cbPrioridad);

		JPanel DatosCaso = new JPanel();
		DatosCaso.setLayout(null);
		DatosCaso.setBackground(Color.WHITE);
		tbbpTicket.addTab("Descripción del caso", null, DatosCaso, null);

		tpDetalle = new JTextPane();
		tpDetalle.setBorder(new LineBorder(new Color(0, 0, 0)));
		tpDetalle.setBounds(10, 88, 786, 81);
		tpDetalle.setFont(new Font("Segoe UI", Font.PLAIN, 16));
		DatosCaso.add(tpDetalle);

		JLabel lblDetalle = new JLabel("Detalle del caso");
		lblDetalle.setBounds(10, 57, 115, 21);
		lblDetalle.setFont(f);
		DatosCaso.add(lblDetalle);

		btnRegistrar = new JButton("Registrar Caso");
		btnRegistrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				LogicaTickets.crearNuevoTicket(nuevoTicket());
				instancia = null;
				dispose();
			}
		});
		btnRegistrar.setFont(f);
		btnRegistrar.setBounds(669, 206, 127, 35);
		DatosCaso.add(btnRegistrar);
		tbbpTicket.setBackgroundAt(1, Color.WHITE);

		JPanel Notas = new JPanel();
		Notas.setBackground(Color.WHITE);
		tbbpTicket.addTab("Notas", null, Notas, null);
		Notas.setLayout(null);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 10, 784, 247);
		Notas.add(scrollPane);

		tblNotas = new JTable();
		tblNotas.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (e.getClickCount() == 2) {
					int id = obtenerId();
					for (Nota n : TicketActivo.getInstancia().getLstNotas()) {
						if (n.getIdNota() == id) {
							CuadroNota.getInstancia().setNota(n);
							CuadroNota.getInstancia().cargarNota();
							CuadroNota.getInstancia().setVisible(true);
							LogicaSistema.seleccionarDescktop().add(CuadroNota.getInstancia());
							CuadroNota.getInstancia().toFront();
						}
					}
				}
			}
		});
		tblNotas.setFont(f);
		tblNotas.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		scrollPane.setViewportView(tblNotas);

		JPopupMenu popupMenu_2 = new JPopupMenu();
		addPopup(tblNotas, popupMenu_2);

		JMenuItem mntmNewMenuItem_5 = new JMenuItem("Abrir Nota");
		mntmNewMenuItem_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int id = obtenerId();
				for (Nota n : TicketActivo.getInstancia().getLstNotas()) {
					if (n.getIdNota() == id) {
						CuadroNota.getInstancia().setNota(n);
						CuadroNota.getInstancia().cargarNota();
						CuadroNota.getInstancia().setVisible(true);
						LogicaSistema.seleccionarDescktop().add(CuadroNota.getInstancia());
						CuadroNota.getInstancia().toFront();
					}
				}
			}
		});
		mntmNewMenuItem_5.setFont(f);
		popupMenu_2.add(mntmNewMenuItem_5);

		JMenuItem mntmNewMenuItem_8 = new JMenuItem("Nueva Nota");
		mntmNewMenuItem_8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CuadroNota.getInstancia().setNota(null);
				CuadroNota.getInstancia().setVisible(true);
				LogicaSistema.seleccionarDescktop().add(CuadroNota.getInstancia());
				CuadroNota.getInstancia().toFront();
			}
		});
		mntmNewMenuItem_8.setFont(f);
		popupMenu_2.add(mntmNewMenuItem_8);

		JMenuItem mntmNewMenuItem_6 = new JMenuItem("Eliminar Nota");
		mntmNewMenuItem_6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int id = obtenerId();
				for (Nota n : TicketActivo.getInstancia().getLstNotas()) {
					if (n.getIdNota() == id) {
						if (n.getUsuario().equals(UsuarioCliente.getInstancia().getUsuario().getNombreUsuario())) {
							LogicaTickets.eliminarNota(id);
						} else {
							JOptionPane.showMessageDialog(LogicaSistema.seleccionarDescktop(),
									"No puede eliminar una nota de otro Usuario", "Warning",
									JOptionPane.WARNING_MESSAGE);
						}
					}
				}
			}
		});
		mntmNewMenuItem_6.setFont(f);
		popupMenu_2.add(mntmNewMenuItem_6);

		JMenuBar menuBar = new JMenuBar();
		menuBar.setFont(f);
		menuBar.setBounds(0, 0, this.getWidth(), 22);
		getContentPane().add(menuBar);

		JMenu mnNewMenu = new JMenu("Ticket");
		mnNewMenu.setFont(f);
		menuBar.add(mnNewMenu);

		JMenuItem mntmNewMenuItem = new JMenuItem("Registrar Solicitud");
		mntmNewMenuItem.setVisible(TicketActivo.getInstancia().getTicket() == null);
		mntmNewMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				LogicaTickets.crearNuevoTicket(nuevoTicket());
				instancia = null;
				dispose();
			}
		});
		mntmNewMenuItem.setFont(f);
		mnNewMenu.add(mntmNewMenuItem);

		miReasignar = new JMenuItem("Reasigar");
		miReasignar.setFont(f);
		miReasignar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Vector<Usuario> lstUsuarios = LogicaTickets
						.lstUsuraiosAsignar(UsuarioCliente.getInstancia().getUsuario().getIdTipoUuario());
				setCursor(new Cursor(Cursor.WAIT_CURSOR));
				JComboBox<String> cbUsuario = LogicaTickets.cbUsuarioAsinar(lstUsuarios);
				JOptionPane.showMessageDialog(LogicaSistema.seleccionarDescktop(), cbUsuario,
						"Seleccione el Usuario a Reasignar el caso", JOptionPane.INFORMATION_MESSAGE);

				if (!cbUsuario.getSelectedItem().toString().equals("")) {
					for (Usuario u : lstUsuarios) {
						if (u.getNombreUsuario().equals(cbUsuario.getSelectedItem().toString())) {
							LogicaTickets.reasignarTicket(u.getIdUsuario());
						}
					}
					LogicaTickets.cambiarEstadoNota(
							new NotaEstado(TicketActivo.getInstancia().getTicket().getIdTicket(), 16));
					setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
					JOptionPane.showMessageDialog(LogicaSistema.seleccionarDescktop(),
							"Usuario reasignado correctamente", "Usuario Reasignado", JOptionPane.INFORMATION_MESSAGE);
					dispose();
				}
			}
		});

		miCalificacion = new JMenuItem("Agregar Calificacion");
		miCalificacion.setFont(f);
		miCalificacion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setCursor(new Cursor(Cursor.WAIT_CURSOR));
				StarRatingPanel srp = new StarRatingPanel();
				JOptionPane.showMessageDialog(LogicaSistema.seleccionarDescktop(), srp, "Califica el Servicio",
						JOptionPane.PLAIN_MESSAGE);
				LogicaTickets.calificarServicio(srp.getRating());
				setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
				JOptionPane.showMessageDialog(LogicaSistema.seleccionarDescktop(),
						"Se asignó correctamente la calificacion", "Calificacion", JOptionPane.INFORMATION_MESSAGE);
			}
		});
		mnNewMenu.add(miCalificacion);
		miReasignar.setFont(f);
		mnNewMenu.add(miReasignar);

		miEscalar = new JMenuItem("Escalar");
		miEscalar.setFont(f);
		miEscalar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				LogicaTickets.escalarCaso(TicketActivo.getInstancia().getTicket().getIdTicket());
				abrirNotaCambiarEstado(new NotaEstado(TicketActivo.getInstancia().getTicket().getIdTicket(), 8));
				dispose();
			}
		});
		miEscalar.setVisible(false);
		miEscalar.setFont(f);
		mnNewMenu.add(miEscalar);

		miCerrarCaso = new JMenuItem("Cerrar Caso");
		miCerrarCaso.setVisible(false);
		miCerrarCaso.setFont(f);
		miCerrarCaso.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				HiloCorreo hc = new HiloCorreo(TicketActivo.getInstancia().getTicket().getUsuarioAutor());
				hc.setAsunto("Aviso de cierre de Ticket");
				hc.setCuerpo("Se informa que se ha realizado el cierre del caso numero "
						+ TicketActivo.getInstancia().getTicket().getNumeroTicket());
				hc.start();
				abrirNotaCambiarEstado(new NotaEstado(TicketActivo.getInstancia().getTicket().getIdTicket(), 10));
				dispose();
			}
		});

		JMenuItem mntmNewMenuItem_7 = new JMenuItem("Solicitar Ampliaciones");
		mntmNewMenuItem_7.setFont(f);
		mntmNewMenuItem_7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				HiloCorreo hc = new HiloCorreo(TicketActivo.getInstancia().getTicket().getUsuarioAutor());
				hc.setAsunto("Solicitud de Ampliar detalle de Ticket");
				hc.setCuerpo("Se solicita ampliar la información de la solicitud indicada por "
						+ "el técnico en el Caso Número " + TicketActivo.getInstancia().getTicket().getNumeroTicket());
				hc.start();
				abrirNotaCambiarEstado(new NotaEstado(TicketActivo.getInstancia().getTicket().getIdTicket(), 11));
			}
		});
		mnNewMenu.add(mntmNewMenuItem_7);
		mnNewMenu.add(miCerrarCaso);

		JMenuItem mntmNewMenuItem_3 = new JMenuItem("Agregar Nota");
		mntmNewMenuItem_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CuadroNota.getInstancia().setNota(null);
				CuadroNota.getInstancia().setVisible(true);
				LogicaSistema.seleccionarDescktop().add(CuadroNota.getInstancia());
				CuadroNota.getInstancia().toFront();
			}
		});

		miAtendido = new JMenuItem("Caso Atendido");
		miAtendido.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				HiloCorreo hc = new HiloCorreo(TicketActivo.getInstancia().getTicket().getUsuarioAutor());
				hc.setAsunto("Aviso de Ticket Atendido");
				hc.setCuerpo("Se informa que se ha finalizado la atención del caso número "
						+ TicketActivo.getInstancia().getTicket().getNumeroTicket());
				hc.start();
				abrirNotaCambiarEstado(new NotaEstado(TicketActivo.getInstancia().getTicket().getIdTicket(), 13));
				dispose();
			}
		});
		miAtendido.setFont(f);
		mnNewMenu.add(miAtendido);

		JMenuItem mntmNewMenuItem_2 = new JMenuItem("Anular Caso");
		mntmNewMenuItem_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				HiloCorreo hc = new HiloCorreo(TicketActivo.getInstancia().getTicket().getUsuarioAutor());
				hc.setAsunto("Aviso de Ticket Anulado");
				hc.setCuerpo("Se informa que se ha realizado la anulacion del caso número "
						+ TicketActivo.getInstancia().getTicket().getNumeroTicket());
				hc.start();
				abrirNotaCambiarEstado(new NotaEstado(TicketActivo.getInstancia().getTicket().getIdTicket(), 9));
				dispose();
			}
		});
		mntmNewMenuItem_2.setFont(f);
		mnNewMenu.add(mntmNewMenuItem_2);

		mntmNewMenuItem_3.setFont(f);
		mnNewMenu.add(mntmNewMenuItem_3);

		JMenuItem mntmNewMenuItem_4 = new JMenuItem("Salir");
		mntmNewMenuItem_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				instancia = null;
				dispose();
			}
		});
		mntmNewMenuItem_4.setFont(f);
		mnNewMenu.add(mntmNewMenuItem_4);

		JMenu mnNewMenu_1 = new JMenu("Bitacora");
		mnNewMenu_1.setFont(f);
		menuBar.add(mnNewMenu_1);

		JMenuItem mntmNewMenuItem_1 = new JMenuItem("Consultar Bitacora");
		mntmNewMenuItem_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (!LogicaSistema.estaAbierto(LogicaSistema.seleccionarDescktop(), "CuadroBitacora")) {
					CuadroBitacora.getInstancia().setVisible(true);
					CuadroBitacora.getInstancia().llenarBitacora();
					LogicaSistema.seleccionarDescktop().add(CuadroBitacora.getInstancia());
					CuadroBitacora.getInstancia().toFront();
				}
			}
		});
		mntmNewMenuItem_1.setFont(f);
		mnNewMenu_1.add(mntmNewMenuItem_1);

	}

	protected int obtenerId() {
		return Integer.parseInt(tblNotas.getModel().getValueAt(tblNotas.getSelectedRow(), 0).toString());
	}

	private DefaultComboBoxModel<String> dcbModelCateforias() {
		DefaultComboBoxModel<String> modelo = new DefaultComboBoxModel<String>();
		Set<String> hashSet = new HashSet<String>();

		for (Producto p : HiloProductos.lstProductos) {
			hashSet.add(p.getCategoria());
		}
		modelo.addElement("");
		for (String elemento : hashSet) {
			modelo.addElement(elemento);
		}
		return modelo;
	}

	private DefaultComboBoxModel<String> dcbModelProductos() {
		DefaultComboBoxModel<String> modelo = new DefaultComboBoxModel<String>();

		modelo.addElement("");
		for (Producto p : HiloProductos.lstProductos) {
			if (p.getCategoria().equals(cbTipoProducto.getSelectedItem().toString())) {
				modelo.addElement(p.getNombreProducto());
			}
		}
		return modelo;
	}

	public void llenarCampos() {
		if (instancia != null) {
			cbTipoProducto.setSelectedItem(ticket.getCategoria());
			cbNombreProducto.setSelectedItem(ticket.getTipoServicio());
			tpDetalle.setText(ticket.getDetalleSolicitud());
			cbPrioridad.setSelectedItem(ticket.getPrioridad());
			tfUsuarioCreador.setText(ticket.getUsuarioAutor());
			tfFechaSolicitud.setText(new SimpleDateFormat("dd/MM/yyyy hh:mm").format(ticket.getFechaSolicutd()));
			tfNumeroTicket.setText(ticket.getNumeroTicket());
			tfFechaCierre.setText(ticket.getFechaCierre() == null ? ""
					: new SimpleDateFormat("dd/MM/yyyy hh:mm").format(ticket.getFechaCierre()));
			tfUsuarioAsignado.setText(ticket.getUsuarioAsignado());
			lblEstado.setText(ticket.getEstado());
		}
	}

	private Ticket nuevoTicket() {
		Ticket t = new Ticket();
		t.setCategoria(cbTipoProducto.getSelectedItem().toString());
		t.setTipoServicio(cbNombreProducto.getSelectedItem().toString());
		t.setDetalleSolicitud(tpDetalle.getText());
		t.setPrioridad(cbPrioridad.getSelectedItem().toString());
		t.setUsuarioAutor(UsuarioCliente.getInstancia().getUsuario().getNombreUsuario());
		t.setIdUsuario(UsuarioCliente.getInstancia().getUsuario().getIdUsuario());

		return t;
	}

	public Ticket getTicket() {
		return ticket;
	}

	public void setTicket(Ticket ticket) {
		this.ticket = ticket;
	}

	public void deshabilitarCampos() {
		cbTipoProducto.setEditable(false);
		cbNombreProducto.setEditable(false);
		tpDetalle.setEditable(false);
		cbPrioridad.setEditable(false);
		tfUsuarioCreador.setEditable(false);
		tfFechaSolicitud.setEditable(false);
		tfNumeroTicket.setEditable(false);
		tfFechaCierre.setEditable(false);
		tfUsuarioAsignado.setEditable(false);
		btnRegistrar.setVisible(false);

		boolean b = (!UsuarioCliente.getInstancia().getUsuario().getSistemaSeleccionado().equals("Usuario"));
		miReasignar.setVisible(b);
		miEscalar.setVisible(b);
		miCerrarCaso.setVisible(b);
		miAtendido.setVisible(b);
		miCalificacion.setVisible(
				(TicketActivo.getInstancia().getTicket().getIdEstado() == 13) && (TicketActivo.getInstancia()
						.getTicket().getIdUsuario() == UsuarioCliente.getInstancia().getUsuario().getIdUsuario()));
	}

	public static void cerrar() {
		instancia.dispose();
		instancia = null;
	}

	protected void abrirNotaCambiarEstado(NotaEstado notaEstado) {
		CuadroNota.getInstancia().setNotaEstado(notaEstado);
		CuadroNota.getInstancia().setNota(null);
		CuadroNota.getInstancia().setVisible(true);
		LogicaSistema.seleccionarDescktop().add(CuadroNota.getInstancia());
		CuadroNota.getInstancia().toFront();
	}

	private static void addPopup(Component component, final JPopupMenu popup) {
		component.addMouseListener(new MouseAdapter() {

			@SuppressWarnings("deprecation")
			@Override
			public void mousePressed(MouseEvent e) {
				if (e.getButton() == 3) {
					try {
						Robot bot = new Robot();
						bot.mousePress(InputEvent.BUTTON1_MASK);
						bot.mouseRelease(InputEvent.BUTTON1_MASK);
					} catch (AWTException ex) {

					}
				}
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}

			private void showMenu(MouseEvent e) {
				popup.show(e.getComponent(), e.getX(), e.getY());
			}
		});
	}
}
