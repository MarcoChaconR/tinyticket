package presentacion;

public class WindowsLookFeel {

	public WindowsLookFeel() {
		super();
	}
	
	public void run() {
		try {
			for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
				if (("Windows".equals(info.getName())) || ("GTK+".equals(info.getName()))) {
					javax.swing.UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
				| javax.swing.UnsupportedLookAndFeelException ex) {
			java.util.logging.Logger.getLogger(WindowsLookFeel.class.getName()).log(java.util.logging.Level.SEVERE,
					null, ex);
		}
	}

}
