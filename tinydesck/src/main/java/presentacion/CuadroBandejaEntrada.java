package presentacion;

import java.awt.AWTException;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Robot;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JInternalFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.InternalFrameAdapter;
import javax.swing.event.InternalFrameEvent;

import entidades.NotaEstado;
import hilos.HiloCambiarEstado;
import hilos.HiloCorreo;
import logicaNegocio.LogicaSistema;
import logicaNegocio.LogicaTickets;

@SuppressWarnings("serial")
public class CuadroBandejaEntrada extends JInternalFrame {
	private static CuadroBandejaEntrada instancia;
	private JTable tbBandeja;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CuadroBandejaEntrada frame = new CuadroBandejaEntrada();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public static CuadroBandejaEntrada getInstancia() {
		if (instancia == null) {
			instancia = new CuadroBandejaEntrada();
		}
		return instancia;
	}

	/**
	 * Create the frame.
	 */
	private CuadroBandejaEntrada() {
		setName("CuadroBandejaEntrada");
		Font f = new Font("Segoe UI", Font.PLAIN, 14);
		addInternalFrameListener(new InternalFrameAdapter() {
			@Override
			public void internalFrameClosed(InternalFrameEvent e) {
				instancia = null;
			}
		});
		getContentPane().setFont(f);

		JScrollPane scrollPane = new JScrollPane();
		getContentPane().add(scrollPane, BorderLayout.CENTER);

		tbBandeja = new JTable();
		tbBandeja.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				boolean ejecutado = false;
				if ((e.getClickCount() == 2) && (!ejecutado)) {
					setCursor(new Cursor(Cursor.WAIT_CURSOR));
					ejecutado = true;
					disparaFrame();
					setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
				}
			}
		});

		tbBandeja.setFont(f);
		tbBandeja.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		scrollPane.setViewportView(tbBandeja);

		JPopupMenu popupMenu_2 = new JPopupMenu();
		addPopup(tbBandeja, popupMenu_2);

		JMenuItem mntmNewMenuItem_2_1 = new JMenuItem("Actualizar Bandeja");
		mntmNewMenuItem_2_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				actualizarBandeja();
			}
		});
		mntmNewMenuItem_2_1.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		popupMenu_2.add(mntmNewMenuItem_2_1);

		JMenuItem mntmNewMenuItem_7 = new JMenuItem("Reasignar Caso");
		mntmNewMenuItem_7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (tbBandeja.getSelectedRow() == -1) {
					JOptionPane.showMessageDialog(LogicaSistema.seleccionarDescktop(),
							"No se encontró ningun registro seleccionado", "Ticket", JOptionPane.WARNING_MESSAGE);
				} else {
					int idTicket = Integer
							.parseInt(tbBandeja.getModel().getValueAt(tbBandeja.getSelectedRow(), 0).toString());
					abrirNotaCambiarEstado(new NotaEstado(idTicket, 16));
				}
			}
		});

		JMenuItem mntmNewMenuItem_8 = new JMenuItem("Abrir Caso");
		mntmNewMenuItem_8.setFont(f);
		mntmNewMenuItem_8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setCursor(new Cursor(Cursor.WAIT_CURSOR));
				disparaFrame();
				setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
			}
		});
		popupMenu_2.add(mntmNewMenuItem_8);
		mntmNewMenuItem_7.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		popupMenu_2.add(mntmNewMenuItem_7);

		JMenuItem mntmNewMenuItem_4_1 = new JMenuItem("Escalar Caso");
		mntmNewMenuItem_4_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (tbBandeja.getSelectedRow() == -1) {
					JOptionPane.showMessageDialog(LogicaSistema.seleccionarDescktop(),
							"No se encontró ningun registro seleccionado", "Ticket", JOptionPane.WARNING_MESSAGE);
				} else {
					int idTicket = Integer
							.parseInt(tbBandeja.getModel().getValueAt(tbBandeja.getSelectedRow(), 0).toString());

					LogicaTickets.escalarCaso(idTicket);
					abrirNotaCambiarEstado(new NotaEstado(idTicket, 8));
				}
			}
		});
		mntmNewMenuItem_4_1.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		popupMenu_2.add(mntmNewMenuItem_4_1);

		JMenuItem mntmNewMenuItem_3_1 = new JMenuItem("Caso Atendido");
		mntmNewMenuItem_3_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (tbBandeja.getSelectedRow() == -1) {
					JOptionPane.showMessageDialog(LogicaSistema.seleccionarDescktop(),
							"No se encontró ningun registro seleccionado", "Ticket", JOptionPane.WARNING_MESSAGE);
				} else {
					int idTicket = Integer
							.parseInt(tbBandeja.getModel().getValueAt(tbBandeja.getSelectedRow(), 0).toString());
					System.out.println(idTicket);
					abrirNotaCambiarEstado(new NotaEstado(idTicket, 13));
				}
			}
		});
		mntmNewMenuItem_3_1.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		popupMenu_2.add(mntmNewMenuItem_3_1);

		JMenuItem mntmNewMenuItem_5_1 = new JMenuItem("Cerrar Caso");
		mntmNewMenuItem_5_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cerrarCaso();
			}
		});
		mntmNewMenuItem_5_1.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		popupMenu_2.add(mntmNewMenuItem_5_1);

		JMenuItem mntmNewMenuItem_6_1 = new JMenuItem("Solicitar Ampliaciones");
		mntmNewMenuItem_6_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				devolverTicket();
			}
		});
		mntmNewMenuItem_6_1.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		popupMenu_2.add(mntmNewMenuItem_6_1);

		setTitle("Bandeja de Entrada Tickets - Usuario Técnico");
		setClosable(true);

		setBounds(100, 100, 960, 540);

		JMenuBar menuBar = new JMenuBar();
		menuBar.setFont(f);
		setJMenuBar(menuBar);

		JMenu mnNewMenu = new JMenu("Caso");
		mnNewMenu.setFont(f);
		menuBar.add(mnNewMenu);

		JMenuItem mntmNewMenuItem_2 = new JMenuItem("Actualizar Bandeja");
		mntmNewMenuItem_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				actualizarBandeja();
			}
		});
		mntmNewMenuItem_2.setFont(f);
		mnNewMenu.add(mntmNewMenuItem_2);

		JMenuItem mntmNewMenuItem = new JMenuItem("Reasignar Caso");
		mntmNewMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (tbBandeja.getSelectedRow() == -1) {
					JOptionPane.showMessageDialog(LogicaSistema.seleccionarDescktop(),
							"No se encontró ningun registro seleccionado", "Ticket", JOptionPane.WARNING_MESSAGE);
				} else {
					int idTicket = Integer
							.parseInt(tbBandeja.getModel().getValueAt(tbBandeja.getSelectedRow(), 0).toString());
					abrirNotaCambiarEstado(new NotaEstado(idTicket, 16));
				}

			}
		});
		mntmNewMenuItem.setFont(f);
		mnNewMenu.add(mntmNewMenuItem);

		JMenuItem mntmNewMenuItem_4 = new JMenuItem("Escalar Caso");
		mntmNewMenuItem_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (tbBandeja.getSelectedRow() == -1) {
					JOptionPane.showMessageDialog(LogicaSistema.seleccionarDescktop(),
							"No se encontró ningun registro seleccionado", "Ticket", JOptionPane.WARNING_MESSAGE);
				} else {
					int idTicket = Integer
							.parseInt(tbBandeja.getModel().getValueAt(tbBandeja.getSelectedRow(), 0).toString());

					LogicaTickets.escalarCaso(idTicket);
					abrirNotaCambiarEstado(new NotaEstado(idTicket, 8));
				}
			}
		});
		mntmNewMenuItem_4.setFont(f);
		mnNewMenu.add(mntmNewMenuItem_4);

		JMenuItem mntmNewMenuItem_3 = new JMenuItem("Caso Atendido");
		mntmNewMenuItem_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				atenderCaso();
			}
		});
		mntmNewMenuItem_3.setFont(f);
		mnNewMenu.add(mntmNewMenuItem_3);

		JMenuItem mntmNewMenuItem_1 = new JMenuItem("Cerrar Bandeja");
		mntmNewMenuItem_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				instancia = null;
				dispose();
			}
		});

		JMenuItem mntmNewMenuItem_5 = new JMenuItem("Cerrar Caso");
		mntmNewMenuItem_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cerrarCaso();
			}
		});

		mntmNewMenuItem_5.setFont(f);
		mnNewMenu.add(mntmNewMenuItem_5);

		JMenuItem mntmNewMenuItem_6 = new JMenuItem("Solicitar Ampliaciones");
		mntmNewMenuItem_6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (tbBandeja.getSelectedRow() == -1) {
					JOptionPane.showMessageDialog(LogicaSistema.seleccionarDescktop(),
							"No se encontró ningun registro seleccionado", "Ticket", JOptionPane.WARNING_MESSAGE);
				} else {
					int idTicket = Integer
							.parseInt(tbBandeja.getModel().getValueAt(tbBandeja.getSelectedRow(), 0).toString());
					abrirNotaCambiarEstado(new NotaEstado(idTicket, 11));
				}
			}
		});
		mntmNewMenuItem_6.setFont(f);
		mnNewMenu.add(mntmNewMenuItem_6);

		mntmNewMenuItem_1.setFont(f);
		mnNewMenu.add(mntmNewMenuItem_1);

	}

	protected void devolverTicket() {
		if (tbBandeja.getSelectedRow() == -1) {
			JOptionPane.showMessageDialog(LogicaSistema.seleccionarDescktop(),
					"No se encontró ningun registro seleccionado", "Ticket", JOptionPane.WARNING_MESSAGE);
		} else {
			int idTicket = Integer.parseInt(tbBandeja.getModel().getValueAt(tbBandeja.getSelectedRow(), 0).toString());
			HiloCorreo hc = new HiloCorreo(tbBandeja.getModel().getValueAt(tbBandeja.getSelectedRow(), 7).toString());
			hc.setAsunto("Solicitud de Ampliar detalle de Ticket");
			hc.setCuerpo(
					"Se solicita ampliar la información de la solicitud indicada por " + "el técnico en el Caso Número "
							+ tbBandeja.getModel().getValueAt(tbBandeja.getSelectedRow(), 1).toString());
			abrirNotaCambiarEstado(new NotaEstado(idTicket, 11));
		}
	}

	protected void atenderCaso() {
		if (tbBandeja.getSelectedRow() == -1) {
			JOptionPane.showMessageDialog(LogicaSistema.seleccionarDescktop(),
					"No se encontró ningun registro seleccionado", "Ticket", JOptionPane.WARNING_MESSAGE);
		} else {
			int idTicket = Integer.parseInt(tbBandeja.getModel().getValueAt(tbBandeja.getSelectedRow(), 0).toString());
			HiloCorreo hc = new HiloCorreo(tbBandeja.getModel().getValueAt(tbBandeja.getSelectedRow(), 7).toString());
			hc.setAsunto("Aviso de Ticket Atendido");
			hc.setCuerpo("Se informa que se ha finalizado la atención del caso número "
					+ tbBandeja.getModel().getValueAt(tbBandeja.getSelectedRow(), 1).toString());
			hc.start();
			abrirNotaCambiarEstado(new NotaEstado(idTicket, 13));
		}
	}

	protected void cerrarCaso() {
		if (tbBandeja.getSelectedRow() == -1) {
			JOptionPane.showMessageDialog(LogicaSistema.seleccionarDescktop(),
					"No se encontró ningun registro seleccionado", "Ticket", JOptionPane.WARNING_MESSAGE);
		} else {
			int idTicket = Integer.parseInt(tbBandeja.getModel().getValueAt(tbBandeja.getSelectedRow(), 0).toString());
			HiloCorreo hc = new HiloCorreo(tbBandeja.getModel().getValueAt(tbBandeja.getSelectedRow(), 7).toString());
			hc.setAsunto("Aviso de cierre de Ticket");
			hc.setCuerpo("Se informa que se ha realizado el cierre del caso numero "
					+ tbBandeja.getModel().getValueAt(tbBandeja.getSelectedRow(), 1).toString());
			hc.start();
			abrirNotaCambiarEstado(new NotaEstado(idTicket, 10));
		}
	}

	protected void abrirNotaCambiarEstado(NotaEstado notaEstado) {
		CuadroNota.getInstancia().setNotaEstado(notaEstado);
		CuadroNota.getInstancia().setNota(null);
		CuadroNota.getInstancia().setVisible(true);
		LogicaSistema.seleccionarDescktop().add(CuadroNota.getInstancia());
		CuadroNota.getInstancia().toFront();
	}

	protected void disparaFrame() {
		int numeroId = traerId();
		HiloCambiarEstado hce = new HiloCambiarEstado(new NotaEstado(numeroId, 7));
		hce.start();
		LogicaTickets.llamarTicket(numeroId);
		FrmTicket.getInstancia().toFront();

	}

	private int traerId() {
		int row = tbBandeja.getSelectedRow();
		return Integer.parseInt(tbBandeja.getModel().getValueAt(row, 0).toString());
	}

	public void actualizarBandeja() {
		int[] anchos = { 17, 90, 19, 175, 79, 98, 69 };
		this.setCursor(new Cursor(Cursor.WAIT_CURSOR));
		tbBandeja.setModel(LogicaTickets.dtmBandejaEntrada());
		tbBandeja.setRowHeight(30);
		for (int i = 0; i < tbBandeja.getModel().getColumnCount() - 1; i++) {
			tbBandeja.getColumnModel().getColumn(i).setPreferredWidth(anchos[i]);
		}
		this.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
	}

	private static void addPopup(Component component, final JPopupMenu popup) {
		component.addMouseListener(new MouseAdapter() {

			@SuppressWarnings("deprecation")
			@Override
			public void mousePressed(MouseEvent e) {
				if (e.getButton() == 3) {
					try {
						Robot bot = new Robot();
						bot.mousePress(InputEvent.BUTTON1_MASK);
						bot.mouseRelease(InputEvent.BUTTON1_MASK);
					} catch (AWTException ex) {

					}
				}
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}

			private void showMenu(MouseEvent e) {
				popup.show(e.getComponent(), e.getX(), e.getY());
			}
		});
	}

}
