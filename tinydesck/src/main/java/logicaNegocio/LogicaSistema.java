package logicaNegocio;

import java.awt.Font;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JDesktopPane;
import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import dataAccess.AccesoDatos;
import entidades.Usuario;
import entidades.UsuarioCliente;
import presentacion.ModuloAdministrador;
import presentacion.ModuloCliente;
import presentacion.ModuloTecnico;

public class LogicaSistema {

	public static boolean abrirSistema(Vector<Usuario> sistema) {

		switch (seleccionarSistema(sistema)) {
		case "Usuario":
			ModuloCliente mc = new ModuloCliente();
			mc.setVisible(true);
			break;
		case "Soporte":
		case "NivelSuperior":
			ModuloTecnico mt = new ModuloTecnico();
			mt.setVisible(true);
			break;
		case "Supervisor":
			// acá va otro módulo de supervisor
			JOptionPane.showMessageDialog(null, "aun falta programar esta parte");
			break;
		case "Administrador":
			ModuloAdministrador ma = new ModuloAdministrador();
			ma.setVisible(true);
			break;
		default:
			return false;
		}
		// System.out.println(UsuarioCliente.getInstancia().getUsuario().toString());
		return true;
	}

	public static String seleccionarSistema(Vector<Usuario> sistema) {
		String sistemaSeleccionado = "";
		DefaultComboBoxModel<String> dcbm = new DefaultComboBoxModel<String>();

		if (sistema.size() == 1) {
			sistemaSeleccionado = sistema.firstElement().getTipoUsuario();
		} else {

			JComboBox<String> cbSistema = new JComboBox<String>();
			cbSistema.setEditable(false);
			cbSistema.setFont(new Font("Roboto Light", Font.PLAIN, 14));
			cbSistema.setToolTipText("Seleccione un Sistema");

			for (Usuario usr : sistema) {
				dcbm.addElement(usr.getTipoUsuario());
			}

			cbSistema.setModel(dcbm);
			JOptionPane.showMessageDialog(null, cbSistema, "Seleccione el módulo de Ingreso",
					JOptionPane.QUESTION_MESSAGE);

			JOptionPane.showMessageDialog(null, "Usuario y Contraseña Correctos\n ¡Bienvenido al Sistema!", "Inicio",
					JOptionPane.INFORMATION_MESSAGE);

			sistemaSeleccionado = cbSistema.getSelectedItem().toString();

			for (Usuario u : sistema) {
				if (u.getTipoUsuario().equals(sistemaSeleccionado)) {
					UsuarioCliente.getInstancia().setUsuario(u);
					UsuarioCliente.getInstancia().getUsuario().setSistemaSeleccionado(sistemaSeleccionado);
				}
			}

		}
		return sistemaSeleccionado;
	}

	public static void cambiarContraseña(String nuevaContrasema) {
		AccesoDatos accesoDatos = new AccesoDatos();
		try {
			accesoDatos.conectar();
			Vector<Object> parametros = new Vector<Object>();
			parametros.add(UsuarioCliente.getInstancia().getUsuario().getNombreUsuario());
			parametros.add(nuevaContrasema);
			accesoDatos.ejecutarStoredProcedure("u902958937_TikectDesk.sp_cambiarContrasena(?,?)", parametros);
			JOptionPane.showMessageDialog(null, "Se realizó el cambio de Contraseña", "Inicio",
					JOptionPane.INFORMATION_MESSAGE);
			accesoDatos.desconectar();
		} catch (SQLException e1) {
			JOptionPane.showMessageDialog(null, "No se obtuvo coneccion con la base de datos", "Error",
					JOptionPane.ERROR_MESSAGE);
		}

	}

	public static JDesktopPane seleccionarDescktop() {
		switch (UsuarioCliente.getInstancia().getUsuario().getSistemaSeleccionado()) {
		case "Usuario":
			return ModuloCliente.desktopPane;
		case "Soporte":
		case "NivelSuperior":
			return ModuloTecnico.desktopPane;
		case "Supervisor":
			// acá va otro módulo de supervisor
			JOptionPane.showMessageDialog(null, "aun falta programar esta parte");
			return null;
		case "Administrador":
			return ModuloAdministrador.desktopPane;
		default:
			return null;
		}
	}

	public static boolean estaAbierto(JDesktopPane descktop, String name) {
		for (JInternalFrame jif : descktop.getAllFrames()) {
			if (jif.getName().equals(name)) {
				return true;
			}
		}
		return false;
	}

	public static void agregarProducto() {
		String tipo = seleccionarTipoProducto();
		String nombre = seleccionarNombreProducto();
		String sqlString = "INSERT INTO u902958937_TikectDesk.tbl_Producto (nombreProducto, tipoProducto) VALUES (?, ?)";
		AccesoDatos ac = new AccesoDatos();
		try {
			ac.conectar();
			String[] parametros = { nombre, tipo };
			ac.ejecutarActualizacion(sqlString, parametros);
			ac.desconectar();
			JOptionPane.showMessageDialog(null, "Producto incluido Satisfactoriamente", "Nuevo Producto",
					JOptionPane.INFORMATION_MESSAGE);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private static String seleccionarNombreProducto() {
		Font f = new Font("Roboto Light", Font.PLAIN, 14);
		JTextField tfNombreProducto = new JTextField();
		tfNombreProducto.setFont(f);

		JOptionPane.showMessageDialog(null, tfNombreProducto, "Seleccione el Tipo de Producto",
				JOptionPane.QUESTION_MESSAGE);

		return tfNombreProducto.getText();
	}

	private static String seleccionarTipoProducto() {
		Font f = new Font("Roboto Light", Font.PLAIN, 14);
		JComboBox<String> cbTipoProducto = new JComboBox<String>();
		cbTipoProducto.setFont(f);
		cbTipoProducto.setEditable(true);
		cbTipoProducto.addItem("");

		AccesoDatos ac = new AccesoDatos();
		try {
			ac.conectar();
			ResultSet rs = ac
					.ejecutarConsulta("SELECT tbl_Producto.tipoProducto FROM u902958937_TikectDesk.tbl_Producto "
							+ "GROUP BY tbl_Producto.tipoProducto");
			while (rs.next()) {
				cbTipoProducto.addItem(rs.getString("tipoProducto"));
			}
			ac.desconectar();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		JOptionPane.showMessageDialog(null, cbTipoProducto, "Seleccione el Tipo de Producto",
				JOptionPane.QUESTION_MESSAGE);

		return cbTipoProducto.getSelectedItem().toString();
	}

	public static void eliminarProductos() {
		String tp = seleccionarProductoEliminar();

		if (JOptionPane.showConfirmDialog(null, "Está seguro de eliminar el producto:\n" + tp, "Eliminar Producto",
				JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
			AccesoDatos ac = new AccesoDatos();
			try {
				ac.conectar();
				ResultSet rs = ac.ejecutarConsulta("SELECT productoID as IdProducto, tipoProducto, nombreProducto "
						+ "FROM u902958937_TikectDesk.tbl_Producto;");
				while (rs.next()) {
					if ((rs.getString("tipoProducto") + ", " + rs.getString("nombreProducto")).equals(tp)) {
						String sqlString = "DELETE FROM u902958937_TikectDesk.tbl_Producto WHERE productoID = ?";
						String[] parametros = { rs.getString("IdProducto") };
						ac.ejecutarActualizacion(sqlString, parametros);
					}
				}
				ac.desconectar();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	private static String seleccionarProductoEliminar() {
		Font f = new Font("Roboto Light", Font.PLAIN, 14);
		JComboBox<String> cbTipoProducto = new JComboBox<String>();
		cbTipoProducto.setFont(f);
		cbTipoProducto.setEditable(true);
		cbTipoProducto.addItem("");

		AccesoDatos ac = new AccesoDatos();
		try {
			ac.conectar();
			ResultSet rs = ac
					.ejecutarConsulta("SELECT tipoProducto, nombreProducto FROM u902958937_TikectDesk.tbl_Producto;");
			while (rs.next()) {
				cbTipoProducto.addItem(rs.getString("tipoProducto") + ", " + rs.getString("nombreProducto"));
			}
			ac.desconectar();

		} catch (SQLException e) {
			e.printStackTrace();
		}

		JOptionPane.showMessageDialog(null, cbTipoProducto, "Seleccione el Tipo de Producto",
				JOptionPane.QUESTION_MESSAGE);

		return cbTipoProducto.getSelectedItem().toString();
	}

}
