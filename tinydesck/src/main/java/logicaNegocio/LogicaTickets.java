package logicaNegocio;

import java.awt.Font;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

import dataAccess.AccesoDatos;
import entidades.Busqueda;
import entidades.ConstruirTicket;
import entidades.Nota;
import entidades.NotaEstado;
import entidades.Producto;
import entidades.Ticket;
import entidades.TicketActivo;
import entidades.Usuario;
import entidades.UsuarioCliente;
import hilos.HiloCorreo;
import hilos.HiloExportarXLS;
import hilos.HiloNotas;
import hilos.HiloProductos;
import presentacion.FrmTicket;

public class LogicaTickets {
	public static DefaultTableModel dtmBuscarTicketsCliente() {
		DefaultTableModel modelo = new DefaultTableModel();
		AccesoDatos ac = new AccesoDatos();
		ResultSet rs;
		try {
			ac.conectar();
			String llave = (Busqueda.getInstancia().getLlaveBusqueda().equals("Estado") ? "descripcionEstado"
					: Busqueda.getInstancia().getLlaveBusqueda());
			String sqlString = "SELECT tiqueteID as IdTicket, NumeroCaso, Servicio, Categoria, "
					+ "fechaCreacion as FechaCreacion, descripcionEstado as Estado, NombreUsuarioAsignado, "
					+ "fechaCierre as FechaCierre, Prioridad FROM u902958937_TikectDesk.vTickets WHERE " + llave
					+ " LIKE '%" + Busqueda.getInstancia().getCriterioBusqueda() + "%' AND NombreUsuario = '"
					+ UsuarioCliente.getInstancia().getUsuario().getNombreUsuario() + "' ORDER BY IdTicket";
			// System.out.println(sqlString);

			rs = ac.ejecutarConsulta(sqlString);

			Busqueda.getInstancia().setSetResultados(rs);

			modelo = dtmResultSet(rs);

		} catch (SQLException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(LogicaSistema.seleccionarDescktop(),
					"Se perdio la conexión, intente nuevamente o comuniquese con un administrador",
					"Error de comunicacion", JOptionPane.ERROR_MESSAGE);
		}

		return modelo;
	}

	@SuppressWarnings("serial")
	public static DefaultTableModel dtmResultSet(ResultSet rs) throws SQLException {
		DefaultTableModel model = new DefaultTableModel() {
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		int columnCount = rs.getMetaData().getColumnCount();

		// Agregar las columnas al modelo
		for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++) {
			model.addColumn(rs.getMetaData().getColumnLabel(columnIndex));
		}

		// Agregar las filas al modelo
		while (rs.next()) {
			Object[] row = new Object[columnCount];
			for (int i = 0; i < columnCount; i++) {
				row[i] = rs.getObject(i + 1);
			}
			model.addRow(row);
		}
		return model;
	}

	public static DefaultTableModel dtmBuscarTicketsTecnico() {
		DefaultTableModel modelo = new DefaultTableModel();
		AccesoDatos ac = new AccesoDatos();
		ResultSet rs;
		try {
			String llave = (Busqueda.getInstancia().getLlaveBusqueda().equals("Estado") ? "descripcionEstado"
					: Busqueda.getInstancia().getLlaveBusqueda());

			ac.conectar();
			String sqlString = "SELECT tiqueteID as IdTicket, NumeroCaso, Servicio, Categoria, "
					+ "fechaCreacion as FechaCreacion, descripcionEstado as Estado, NombreUsuarioAsignado, "
					+ "fechaCierre as FechaCierre, NombreUsuario, Prioridad FROM u902958937_TikectDesk.vTickets WHERE "
					+ llave + " LIKE '%" + Busqueda.getInstancia().getCriterioBusqueda() + "%' ORDER BY IdTicket";
			// System.out.println(sqlString);

			rs = ac.ejecutarConsulta(sqlString);

			Busqueda.getInstancia().setSetResultados(rs);

			modelo = dtmResultSet(rs);
			ac.desconectar();

		} catch (SQLException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(LogicaSistema.seleccionarDescktop(),
					"Se perdio la conexión, intente nuevamente o comuniquese con un administrador",
					"Error de comunicacion", JOptionPane.ERROR_MESSAGE);
		}

		return modelo;
	}

	public static DefaultTableModel dtmBandejaEntrada() {
		DefaultTableModel modelo = new DefaultTableModel();
		AccesoDatos ac = new AccesoDatos();
		ResultSet rs;
		try {
			ac.conectar();
			String sqlString = "SELECT tiqueteID as IdTicket, NumeroCaso, Prioridad, Servicio, Categoria, "
					+ "fechaCreacion as FechaCreacion, descripcionEstado as Estado, NombreUsuario FROM "
					+ "u902958937_TikectDesk.vTickets where (estadoID = '6' or estadoID = '7' or estadoID = '8' or estadoID = '16') "
					+ "AND (usuarioAsignadoID = '" + UsuarioCliente.getInstancia().getUsuario().getIdUsuario()
					+ "') ORDER BY IdTicket";
			// System.out.println(sqlString);

			rs = ac.ejecutarConsulta(sqlString);

			modelo = dtmResultSet(rs);
			ac.desconectar();

		} catch (SQLException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(LogicaSistema.seleccionarDescktop(),
					"Se perdio la conexión, intente nuevamente o comuniquese con un administrador",
					"Error de comunicacion", JOptionPane.ERROR_MESSAGE);
		}

		return modelo;
	}

	public static void crearNuevoTicket(Ticket nuevoTicket) {
		AccesoDatos ac = new AccesoDatos();
		Vector<Object> parametros = new Vector<Object>();
		HiloCorreo hc = new HiloCorreo(nuevoTicket.getUsuarioAutor());
		hc.setAsunto("Registro de Ticket realizado correctamente");
		hc.setCuerpo("Se informa que se ha realizado el registro satisfactorio de una nueva solicitud de Ticket");
		hc.start();

		try {
			ac.conectar();
			parametros.add(getIdProducto(nuevoTicket.getCategoria(), nuevoTicket.getTipoServicio()));
			parametros.add(nuevoTicket.getIdUsuario());
			parametros.add(nuevoTicket.getDetalleSolicitud());
			parametros.add(idPrioridad(nuevoTicket.getPrioridad()));

			ac.ejecutarStoredProcedure("u902958937_TikectDesk.sp_insertarTiquete(?,?,?,?)", parametros);

			ac.desconectar();
			JOptionPane.showMessageDialog(LogicaSistema.seleccionarDescktop(),
					"Se ha creado el nuevo registro correctamente", "Proceso realizado",
					JOptionPane.INFORMATION_MESSAGE);
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	private static int getIdProducto(String categoria, String producto) {
		for (Producto p : HiloProductos.lstProductos) {
			if ((categoria.equals(p.getCategoria())) && (producto.equals(p.getNombreProducto()))) {
				return p.getIdProducto();
			}
		}
		return 0;
	}

	private static int idPrioridad(String Prioridad) {
		switch (Prioridad) {
		case "Baja":
			return 1;
		case "Media":
			return 2;
		case "Alta":
			return 3;
		default:
			return 0;
		}
	}

	public static void llamarTicket(int id) {
		ConstruirTicket ct = new ConstruirTicket();
		ct.traerDatosBD(id);
		ct.llamarNotas(id);
		ct.llenarFrm();
		ct.bloquearCamposFrm();
		if (LogicaSistema.estaAbierto(LogicaSistema.seleccionarDescktop(), "FrmTicket")) {
			FrmTicket.getInstancia().llenarCampos();
			FrmTicket.getInstancia().toFront();
		} else {
			ct.traerFrmTicket(LogicaSistema.seleccionarDescktop());
		}

	}

	public static void crearNota(Nota nota) {
		AccesoDatos ac = new AccesoDatos();
		String sp = "`u902958937_TikectDesk`.`sp_insertarNota`";
		Vector<Object> parametros = new Vector<Object>();

		parametros.add(nota.getAsunto());
		parametros.add(nota.getDetalleNota());
		parametros.add(nota.getIdUsuario());
		parametros.add(nota.getIdTicket());

		try {
			ac.conectar();
			ac.ejecutarStoredProcedure(sp + "(?,?,?,?)", parametros);
			ac.desconectar();

			HiloNotas hn = new HiloNotas(TicketActivo.getInstancia().getTicket().getIdTicket());
			hn.start();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void eliminarNota(int id) {
		AccesoDatos ac = new AccesoDatos();
		String sp = "`u902958937_TikectDesk`.`sp_AnularNota`";
		Vector<Object> parametros = new Vector<Object>();

		parametros.add(id);

		try {
			ac.conectar();
			ac.ejecutarStoredProcedure(sp + "(?)", parametros);
			ac.desconectar();

			HiloNotas hn = new HiloNotas(TicketActivo.getInstancia().getTicket().getIdTicket());
			hn.start();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void cambiarEstadoNota(NotaEstado notaEstado) {
		AccesoDatos ac = new AccesoDatos();
		String sp = "`u902958937_TikectDesk`.`sp_CambiarEstadoTicket`";
		Vector<Object> parametros = new Vector<Object>();

		parametros.add(notaEstado.getIdEstado());
		parametros.add(notaEstado.getIdTicket());
		parametros.add(UsuarioCliente.getInstancia().getUsuario().getIdUsuario());

		actualizarFechaCierreNota(notaEstado);

		try {
			ac.conectar();
			ac.ejecutarStoredProcedure(sp + "(?,?,?)", parametros);
			ac.desconectar();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private static void actualizarFechaCierreNota(NotaEstado notaEstado) {
		AccesoDatos ac = new AccesoDatos();
		String sqlString = "UPDATE u902958937_TikectDesk.tbl_Tiquete SET fechaCierre = now() where tiqueteID = ?";
		String[] parametros = { String.valueOf(notaEstado.getIdTicket()) };

		try {
			ac.conectar();
			ac.ejecutarActualizacion(sqlString, parametros);
			ac.desconectar();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public static DefaultTableModel dtmBitacora() {
		DefaultTableModel modelo = new DefaultTableModel();
		AccesoDatos ac = new AccesoDatos();
		ResultSet rs;
		try {
			ac.conectar();
			String sqlString = "SELECT IdBitacora as Id, NombreUsuario, ValorAnterior, ValorNuevo, FechaModificacion "
					+ "FROM u902958937_TikectDesk.vBitacora WHERE tiqueteReferenciaID = "
					+ TicketActivo.getInstancia().getTicket().getIdTicket() + " ORDER BY IdBitacora";
			// System.out.println(sqlString);

			rs = ac.ejecutarConsulta(sqlString);

			modelo = dtmResultSet(rs);
			ac.desconectar();

		} catch (SQLException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(LogicaSistema.seleccionarDescktop(),
					"Se perdio la conexión, intente nuevamente o comuniquese con un administrador",
					"Error de comunicacion", JOptionPane.ERROR_MESSAGE);
		}

		return modelo;

	}

	public static Vector<Usuario> lstUsuraiosAsignar(int idTipoUsuario) {
		Vector<Usuario> lstUsr = new Vector<Usuario>();
		AccesoDatos ac = new AccesoDatos();
		ResultSet rs;
		try {
			ac.conectar();
			String sqlString = "select usuarioID, nombreUsuario from u902958937_TikectDesk.tbl_Usuarios"
					+ " where estadoID = 1 AND tipoUsuario = " + idTipoUsuario;
			// System.out.println(sqlString);

			rs = ac.ejecutarConsulta(sqlString);

			while (rs.next()) {
				Usuario u = new Usuario();
				u.setIdUsuario(rs.getInt("usuarioID"));
				u.setNombreUsuario(rs.getString("nombreUsuario"));
				lstUsr.add(u);
				u = null;

			}
			ac.desconectar();

		} catch (SQLException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(LogicaSistema.seleccionarDescktop(),
					"Se perdio la conexión, intente nuevamente o comuniquese con un administrador",
					"Error de comunicacion", JOptionPane.ERROR_MESSAGE);
		}

		return lstUsr;
	}

	public static JComboBox<String> cbUsuarioAsinar(Vector<Usuario> lstUsuarios) {
		DefaultComboBoxModel<String> dcbm = new DefaultComboBoxModel<String>();

		JComboBox<String> cbUsuarios = new JComboBox<String>();
		cbUsuarios.setEditable(false);
		cbUsuarios.setFont(new Font("Roboto Light", Font.PLAIN, 14));
		cbUsuarios.setToolTipText("Seleccione un Sistema");

		dcbm.addElement("");
		for (Usuario usr : lstUsuarios) {
			dcbm.addElement(usr.getNombreUsuario());
		}
		cbUsuarios.setModel(dcbm);

		return cbUsuarios;
	}

	public static void reasignarTicket(int idUsuario) {
		AccesoDatos ac = new AccesoDatos();
		String sp = "`u902958937_TikectDesk`.`sp_Reasignar`";
		Vector<Object> parametros = new Vector<Object>();

		parametros.add(TicketActivo.getInstancia().getTicket().getIdTicket());
		parametros.add(idUsuario);

		try {
			ac.conectar();
			ac.ejecutarStoredProcedure(sp + "(?,?)", parametros);
			ac.desconectar();

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public static void escalarCaso(int idTicket) {
		AccesoDatos ac = new AccesoDatos();
		String sp = "`u902958937_TikectDesk`.`sp_EscalarCaso`";
		Vector<Object> parametros = new Vector<Object>();

		parametros.add(UsuarioCliente.getInstancia().getUsuario().getIdTipoUuario() + 1);
		parametros.add(idTicket);

		try {
			ac.conectar();
			ac.ejecutarStoredProcedure(sp + "(?,?)", parametros);
			ac.desconectar();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void calificarServicio(int calificacion) {
		AccesoDatos ac = new AccesoDatos();
		String sp = "`u902958937_TikectDesk`.`sp_CalificarServicio`";
		Vector<Object> parametros = new Vector<Object>();

		parametros.add(TicketActivo.getInstancia().getTicket().getIdTicket());
		parametros.add(calificacion);

		try {
			ac.conectar();
			ac.ejecutarStoredProcedure(sp + "(?,?)", parametros);
			ac.desconectar();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public static void exportarXLS(String sqlString) {
		HiloExportarXLS hexls = new HiloExportarXLS(sqlString);
		hexls.start();
	}

}
