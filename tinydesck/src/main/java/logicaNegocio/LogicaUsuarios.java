package logicaNegocio;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Random;
import java.util.Vector;

import javax.swing.JOptionPane;

import dataAccess.AccesoDatos;
import entidades.Usuario;
import hilos.HiloCorreo;

public class LogicaUsuarios {
	public static boolean validarUsuario(String nombreUsuario, String contrasena, int intentos) {
		AccesoDatos accesoDatos = new AccesoDatos();
		Vector<Usuario> listaSistema = new Vector<Usuario>();

		try {
			accesoDatos.conectar();
			Vector<Object> parametros = new Vector<Object>();

			parametros.add(nombreUsuario);
			parametros.add(contrasena);

			ResultSet rs = accesoDatos.ejecutarStoredProcedure("u902958937_TikectDesk.sp_consultar_Usuario(?,?)",
					parametros);

			while (rs.next()) {
				Usuario u = new Usuario();
				u.setIdUsuario(rs.getInt("usuarioID"));
				u.setTipoUsuario(rs.getString("tipoUsuario"));
				u.setIdTipoUuario(rs.getInt("tipoUsuarioID"));
				u.setDepartamento(rs.getString("nombreDepartamento"));
				u.setCorreoUsuario(rs.getString("correoUsuario"));
				u.setEstado(rs.getString("descripcionEstado"));
				u.setNombreUsuario(rs.getString("nombreUsuario"));
				u.setFechaIngreso(new SimpleDateFormat("dd/MM/yyyy").format(rs.getDate("fechaIngreso")));
				u.setNumeroEmpleado(rs.getString("numeroEmpleado"));
				u.setContrasena(contrasena);
				listaSistema.add(u);

				u = null;
			}
			accesoDatos.desconectar();

			if (listaSistema.size() != 0) {
				return LogicaSistema.abrirSistema(listaSistema);
			} else {
				if (intentos > 3) {
					accesoDatos.conectar();
					parametros.clear();
					parametros.add(nombreUsuario);
					accesoDatos.ejecutarStoredProcedure("u902958937_TikectDesk.sp_bloquearUsuario(?)", parametros);
					JOptionPane.showMessageDialog(null, "Usuario bloqueado, debe comunicarse con un Administrador",
							"Inicio", JOptionPane.ERROR_MESSAGE);
					accesoDatos.conectar();
				} else {
					JOptionPane.showMessageDialog(null, "Usuario y Contraseña invalidos, intente nuevamente", "Inicio",
							JOptionPane.ERROR_MESSAGE);
				}
			}

		} catch (SQLException ex) {
			ex.printStackTrace();
			return false;
		}
		return false;
	}

	public static void registrarNuevoUsuario(String correo, String contrasena) {
		Random r = new Random();
		AccesoDatos ac = new AccesoDatos();
		String sqlString = "INSERT INTO u902958937_TikectDesk.tbl_Usuarios (nombreUsuario, correoUsuario, contrasena, "
				+ "estadoID, departamentoID, tipoUsuario, numeroEmpleado) VALUES (?, ?, ?, 1, 1, 1, ?)";
		try {
			ac.conectar();
			String[] parametros = { correo, correo, contrasena, String.valueOf(r.nextInt(5)) };
			ac.ejecutarActualizacion(sqlString, parametros);
			
			HiloCorreo hc = new HiloCorreo(correo);
			hc.setAsunto("Se ha registrado su nuevo Usuario");
			hc.setCuerpo("Bienvenido al sistema TinyDesck, se le informa que se ha registrado "
					+ "satisfactoriamente el usuario " + correo);
			hc.start();
			
			JOptionPane.showMessageDialog(null,
					"Se registró correctamente el usuario, ingrese nuevamente los datos de inicio",
					"Usuario Registrado", JOptionPane.INFORMATION_MESSAGE);
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public static boolean usuarioSinRegistrar(String text) {
		AccesoDatos ac = new AccesoDatos();

		try {
			ac.conectar();
			String sqlString = "SELECT usuarioID as IdUsuario from u902958937_TikectDesk.tbl_Usuarios "
					+ "where nombreUsuario = '" + text + "' or correoUsuario = '" + text + "'";
			ResultSet rs = ac.ejecutarConsulta(sqlString);
			if (rs.next()) {
				return true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return false;
	}

}
