package hilos;

import java.awt.Desktop;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

import javax.swing.JOptionPane;

import dataAccess.AccesoDatos;

public class HiloExportarXLS extends Thread {
	private String columnas[];
	private String sqlString;

	public HiloExportarXLS(String SQLString) {
		super();
		this.sqlString = SQLString;
	}

	@Override
	public void run() {
		String cuerpo = "";
		boolean continuar = true;

		if (this.columnas == null) {

			File tempFile = null;
			try {
				tempFile = File.createTempFile("Listado", ".xls");
				tempFile.deleteOnExit();
			} catch (IOException e) {
				tempFile = null;
			}
			BufferedWriter bw;
			String campo = "";

			if (continuar == true) {
				AccesoDatos c = new AccesoDatos();

				try {
					c.conectar();
					bw = new BufferedWriter(
							new OutputStreamWriter(new FileOutputStream(tempFile.getAbsolutePath()), "Cp1252"));
					ResultSet rs = c.ejecutarConsulta(this.sqlString);

					ResultSetMetaData rsmd = rs.getMetaData();

					for (int i = 0; i < rsmd.getColumnCount(); i++) {
						cuerpo += rsmd.getColumnLabel(i + 1) + "\t";
					}
					columnas = cuerpo.split("\t");

					bw.write(cuerpo);
					bw.newLine();

					cuerpo = "";

					while (rs.next()) {
						for (String col : columnas) {
							try {
								campo = rs.getString(col).trim().replace("\t", "").replace("\n", " ").replace("\r",
										" ");
							} catch (Exception e) {
								campo = "";
							}
							cuerpo += campo + "\t";
						}
						bw.write(cuerpo);
						bw.newLine();

						cuerpo = "";
					}

					bw.close();
					Desktop.getDesktop().open(tempFile);

				} catch (IOException | SQLException e1) {
					e1.printStackTrace();
					JOptionPane.showMessageDialog(null, e1.getMessage());
				}
			}
		}
	}
}
