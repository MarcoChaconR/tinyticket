package hilos;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.mail.MessagingException;

import dataAccess.AccesoDatos;
import entidades.Email;

public class HiloCorreo extends Thread {
	private String asunto;
	private String cuerpo;
	private String usuarioDestinatario;

	public HiloCorreo(String UsuarioDestino) {
		super();
		this.usuarioDestinatario = UsuarioDestino;
	}

	@Override
	public void run() {
		Email email = new Email(getCorreoUsuarioBD(this.usuarioDestinatario), asunto, cuerpo);
		try {
			email.send();
		} catch (MessagingException e) {
			e.printStackTrace();
		}
		System.out.println(email.toString());
	}

	private String getCorreoUsuarioBD(String usuario) {
		AccesoDatos ac = new AccesoDatos();
		ResultSet rs;
		String correo = "";
		try {
			ac.conectar();
			String sqlString = "SELECT correoUsuario FROM u902958937_TikectDesk.tbl_Usuarios WHERE nombreUsuario = '"
					+ usuario + "' limit 1";
			rs = ac.ejecutarConsulta(sqlString);

			if (rs.next()) {
				correo = rs.getString("correoUsuario");
			}

		} catch (SQLException e) {
			e.printStackTrace();

		}

		return correo;

	}

	public String getAsunto() {
		return asunto;
	}

	public void setAsunto(String asunto) {
		this.asunto = asunto;
	}

	public String getCuerpo() {
		return cuerpo;
	}

	public void setCuerpo(String cuerpo) {
		this.cuerpo = cuerpo;
	}

	public String getUsuarioDestinatario() {
		return usuarioDestinatario;
	}

	public void setUsuarioDestinatario(String usuarioDestinatario) {
		this.usuarioDestinatario = usuarioDestinatario;
	}
}
