package hilos;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

import javax.swing.JOptionPane;

import dataAccess.AccesoDatos;
import entidades.Producto;

public class HiloProductos extends Thread {
	public static Vector<Producto> lstProductos = new Vector<Producto>();

	public HiloProductos() {
		super();
	}

	@Override
	public void run() {
		AccesoDatos ac = new AccesoDatos();
		String sqlString = "SELECT productoID as IdProducto, nombreProducto as NombreProducto,"
				+ " tipoProducto as Categoria FROM u902958937_TikectDesk.tbl_Producto";
		lstProductos.clear();

		try {
			ac.conectar();
			ResultSet rs = ac.ejecutarConsulta(sqlString);

			while (rs.next()) {
				Producto pdt = new Producto();
				pdt.setIdProducto(rs.getInt("IdProducto"));
				pdt.setCategoria(rs.getString("Categoria"));
				pdt.setNombreProducto(rs.getString("NombreProducto"));

				lstProductos.add(pdt);
				pdt = null;
			}
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Se perdió la conexión con la base de datos", "Error de conexión",
					JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}
		
	}
}
