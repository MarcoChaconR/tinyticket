package hilos;

import entidades.NotaEstado;
import logicaNegocio.LogicaTickets;

public class HiloCambiarEstado extends Thread {
	private NotaEstado notaEstado;

	public HiloCambiarEstado(NotaEstado notaEstado) {
		super();
		this.notaEstado = notaEstado;
	}

	@Override
	public void run() {
		LogicaTickets.cambiarEstadoNota(notaEstado);
	}

}
