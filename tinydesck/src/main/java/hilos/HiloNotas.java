package hilos;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

import javax.swing.table.DefaultTableModel;

import dataAccess.AccesoDatos;
import entidades.Nota;
import entidades.TicketActivo;
import presentacion.FrmTicket;

public class HiloNotas extends Thread {
	private int idNota;

	public HiloNotas(int id) {
		super();
		this.idNota = id;
	}

	@Override
	public void run() {
		AccesoDatos ac = new AccesoDatos();
		Vector<Nota> notas = new Vector<Nota>();
		try {
			ac.conectar();
			ResultSet rs = ac.ejecutarConsulta("SELECT IdNota, Asunto, DetalleNota, FechaNota, "
					+ "NombreUsuario as Usuario from u902958937_TikectDesk.vNotas where IdTicket = " + idNota
					+ " AND estadoID = 14");

			while (rs.next()) {
				Nota n = new Nota();
				n.setAsunto(rs.getString("Asunto"));
				n.setDetalleNota(rs.getString("DetalleNota"));
				n.setUsuario(rs.getString("Usuario"));
				n.setIdNota(rs.getInt("IdNota"));
				n.setFecha(rs.getString("FechaNota"));

				notas.add(n);
				n = null;
			}
			FrmTicket.getInstancia().tblNotas.setModel(dtmNotas(notas));
			FrmTicket.getInstancia().tblNotas.setRowHeight(30);
			TicketActivo.getInstancia().setLstNotas(notas);

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public int getIdNota() {
		return idNota;// idTicket
	}

	public void setIdNota(int idNota) {
		this.idNota = idNota;// idTicket
	}

	@SuppressWarnings("serial")
	public static DefaultTableModel dtmNotas(Vector<Nota> notas) throws SQLException {
		DefaultTableModel model = new DefaultTableModel() {
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};

		String[] columnas = { "IdNota", "Asunto", "DetalleNota", "FechaNota", "Usuario" };

		// Agregar las columnas al modelo
		for (String col : columnas) {
			model.addColumn(col);
		}

		// Agregar las filas al modelo
		for (Nota n : notas) {
			Object[] f = new Object[columnas.length];
			f[0] = n.getIdNota();
			f[1] = n.getAsunto();
			f[2] = n.getDetalleNota();
			f[3] = n.getFecha();
			f[4] = n.getUsuario();

			model.addRow(f);
		}

		return model;
	}

}
