package entidades;

public class Usuario {
	private int idUsuario;
	private String nombreUsuario;
	private String correoUsuario;
	private String contrasena;
	private String fechaIngreso;
	private String estado;
	private String departamento;
	private int idTipoUuario;
	private String tipoUsuario;
	private String numeroEmpleado;
	private String sistemaSeleccionado;

	public Usuario() {
		super();
	}

	public int getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getNombreUsuario() {
		return nombreUsuario;
	}

	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}

	public String getCorreoUsuario() {
		return correoUsuario;
	}

	public void setCorreoUsuario(String correoUsuario) {
		this.correoUsuario = correoUsuario;
	}

	public String getContrasena() {
		return contrasena;
	}

	public void setContrasena(String contrasena) {
		this.contrasena = contrasena;
	}

	public String getFechaIngreso() {
		return fechaIngreso;
	}

	public void setFechaIngreso(String fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getDepartamento() {
		return departamento;
	}

	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}

	public String getTipoUsuario() {
		return tipoUsuario;
	}

	public void setTipoUsuario(String tipoUsuario) {
		this.tipoUsuario = tipoUsuario;
	}

	public String getNumeroEmpleado() {
		return numeroEmpleado;
	}

	public void setNumeroEmpleado(String numeroEmpleado) {
		this.numeroEmpleado = numeroEmpleado;
	}

	@Override
	public String toString() {
		return "Usuario [idUsuario=" + idUsuario + ", nombreUsuario=" + nombreUsuario + ", correoUsuario="
				+ correoUsuario + ", contrasena=" + contrasena + ", fechaIngreso=" + fechaIngreso + ", estado=" + estado
				+ ", departamento=" + departamento + ", tipoUsuario=" + tipoUsuario + ", numeroEmpleado="
				+ numeroEmpleado + "]";
	}

	public String getSistemaSeleccionado() {
		return sistemaSeleccionado;
	}

	public void setSistemaSeleccionado(String sistemaSeleccionado) {
		this.sistemaSeleccionado = sistemaSeleccionado;
	}

	public int getIdTipoUuario() {
		return idTipoUuario;
	}

	public void setIdTipoUuario(int idTipoUuario) {
		this.idTipoUuario = idTipoUuario;
	}
}
