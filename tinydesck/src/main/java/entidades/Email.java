package entidades;

import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class Email {
	private String destinatario;
	private String from;
	private String asunto;
	private String cuerpo;

	public Email(String para, String asunto, String cuerpo) {
		this.destinatario = para;
		this.from = "soporte@craura.com";
		this.asunto = asunto;
		this.cuerpo = cuerpo;
	}

	public void send() throws MessagingException {
		// Configuración de las propiedades del servidor SMTP
		Properties props = new Properties();
		props.put("mail.smtp.host", "smtp.hostinger.com"); // Servidor SMTP de Hostinger
		// props.put("mail.smtp.port", "465"); // Puerto SMTP para TLS
		props.put("mail.smtp.port", "587"); // Puerto SMTP para TLS
		props.put("mail.smtp.auth", "true"); // Autenticación requerida
		props.put("mail.smtp.starttls.enable", "true"); // Requerido para el cifrado TLS

		// Crear la sesión del correo electrónico con las credenciales
		Session session = Session.getInstance(props, new Authenticator() {
			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication("Soporte@craura.com", "Soporte123/");
			}
		});

		// Crear el mensaje de correo electrónico
		Message message = new MimeMessage(session);
		message.setFrom(new InternetAddress(from));
		message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(destinatario));
		message.setSubject(asunto);
		message.setText(cuerpo);

		// Enviar el mensaje de correo electrónico
		Transport.send(message);
	}

	@Override
	public String toString() {
		return "de: Soporte@craura.com, para: " + destinatario + ", Asunto: " + asunto + ", Cuerpo: " + cuerpo;
	}
}
