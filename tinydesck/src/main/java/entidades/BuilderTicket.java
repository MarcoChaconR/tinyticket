package entidades;

import javax.swing.JDesktopPane;

public interface BuilderTicket {
	public void traerDatosBD(int idTicket);

	public void llamarNotas(int idTicket);

	public void traerFrmTicket(JDesktopPane desktopPane);

	public void llenarFrm();

	public void bloquearCamposFrm();

}
