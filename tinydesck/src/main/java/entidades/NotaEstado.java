package entidades;

public class NotaEstado {
	private int idTicket;
	private int idEstado;

	public NotaEstado() {
		super();
	}

	public NotaEstado(int IdTicket, int IdEstado) {
		super();
		this.idTicket = IdTicket;
		this.idEstado = IdEstado;
	}

	public int getIdTicket() {
		return idTicket;
	}

	public void setIdTicket(int idTicket) {
		this.idTicket = idTicket;
	}

	public int getIdEstado() {
		return idEstado;
	}

	public void setIdEstado(int idEstado) {
		this.idEstado = idEstado;
	}

}
