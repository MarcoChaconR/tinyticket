package entidades;

import java.awt.Point;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JDesktopPane;
import javax.swing.JInternalFrame;

import dataAccess.AccesoDatos;
import hilos.HiloNotas;
import presentacion.FrmTicket;

public class ConstruirTicket implements BuilderTicket {

	public ConstruirTicket() {
		super();
		TicketActivo.cerrarInstancia();
	}

	@Override
	public void traerDatosBD(int idTicket) {
		AccesoDatos ac = new AccesoDatos();
		ResultSet rs;
		Ticket t = new Ticket();
		try {
			ac.conectar();
			String sqlString = "SELECT tiqueteID as IdTicket, IdServicio, Servicio, Categoria, "
					+ "usuarioCreadorID as IdAutor, NombreUsuario, fechaCreacion as FechaCreacion, "
					+ "estadoId as IdEstado, descripcionEstado as Estado, usuarioAsignadoID as IdAsignado, "
					+ "NombreUsuarioAsignado as Asignado, fechaCierre as FechaCierre, DetalleSolicitud, NumeroCaso, "
					+ "IdPrioridad, Prioridad FROM u902958937_TikectDesk.vTickets where tiqueteID = " + idTicket;
			rs = ac.ejecutarConsulta(sqlString);
			// System.out.println(sqlString);

			if (rs.next()) {
				// mapeo de los datos
				t.setIdTicket(idTicket);
				t.setCategoria(rs.getString("Categoria"));
				t.setDetalleSolicitud(rs.getString("DetalleSolicitud"));
				t.setEstado(rs.getString("Estado"));
				t.setFechaCierre(rs.getDate("FechaCierre"));
				t.setFechaSolicutd(rs.getDate("FechaCreacion"));
				t.setIdEstado(rs.getInt("IdEstado"));
				t.setIdProducto(rs.getInt("IdServicio"));
				t.setIdTicket(rs.getInt("IdTicket"));
				t.setIdUsuario(rs.getInt("IdAutor"));
				t.setNumeroTicket(rs.getString("NumeroCaso"));
				t.setPrioridad(rs.getString("Prioridad"));
				t.setTipoServicio(rs.getString("Servicio"));
				t.setUsuarioAsignado(rs.getString("Asignado"));
				t.setUsuarioAutor(rs.getString("NombreUsuario"));

				TicketActivo.getInstancia().setTicket(t);
				t = null;
				ac.desconectar();
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void llamarNotas(int idTicket) {
		HiloNotas hn = new HiloNotas(idTicket);
		hn.start();
	}

	@Override
	public void traerFrmTicket(JDesktopPane desktopPane) {
		desktopPane.add(FrmTicket.getInstancia());
		FrmTicket.getInstancia().setLocation(p(FrmTicket.getInstancia(), desktopPane));
	}

	@Override
	public void llenarFrm() {
		FrmTicket.getInstancia().setTicket(TicketActivo.getInstancia().getTicket());
		FrmTicket.getInstancia().llenarCampos();
	}

	@Override
	public void bloquearCamposFrm() {
		FrmTicket.getInstancia().deshabilitarCampos();
		FrmTicket.getInstancia().setVisible(true);
		FrmTicket.getInstancia().toFront();
	}

	private Point p(JInternalFrame frame, JDesktopPane desktopPane) {
		return new Point(((int) (desktopPane.getWidth() - frame.getWidth()) / 2),
				((int) (desktopPane.getHeight() - frame.getHeight()) / 2));
	}

}
