package entidades;

public class UsuarioCliente {
	private static UsuarioCliente instancia;
	private Usuario usuario;
	
	private UsuarioCliente() {
		super();
	}
	
	public static UsuarioCliente getInstancia() {
		if(instancia == null) {
			instancia = new UsuarioCliente();
		}
		return instancia;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
}