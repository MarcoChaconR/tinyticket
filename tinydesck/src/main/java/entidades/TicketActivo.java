package entidades;

import java.util.Vector;

public class TicketActivo {
	private static TicketActivo instancia;
	private Ticket ticket;
	private Vector<Nota> lstNotas;

	public static TicketActivo getInstancia() {
		if (instancia == null) {
			instancia = new TicketActivo();
		}
		return instancia;
	}

	private TicketActivo() {
		super();
	}

	public static void cerrarInstancia() {
		instancia = null;
	}

	public Ticket getTicket() {
		return ticket;
	}

	public void setTicket(Ticket ticket) {
		this.ticket = ticket;
	}

	public Vector<Nota> getLstNotas() {
		return lstNotas;
	}

	public void setLstNotas(Vector<Nota> lstNotas) {
		this.lstNotas = lstNotas;
	}

}
