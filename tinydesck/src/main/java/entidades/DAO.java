package entidades;

import java.util.Optional;
import java.util.Vector;

public interface DAO<O> {

	public Vector<O> getVectorRegistros();

	public Optional<O> getRegistroId(int id);

	public void insertRegistro (O objeto);

	public void updateRegistro(O objeto);

	public void deleteRegistro(int id);

}
