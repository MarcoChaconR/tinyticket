package entidades;

public class Departamento {
	private int idDepartamento;
	private int nombreDepartamento;

	public Departamento() {
		super();
	}

	public int getIdDepartamento() {
		return idDepartamento;
	}

	public void setIdDepartamento(int idDepartamento) {
		this.idDepartamento = idDepartamento;
	}

	public int getNombreDepartamento() {
		return nombreDepartamento;
	}

	public void setNombreDepartamento(int nombreDepartamento) {
		this.nombreDepartamento = nombreDepartamento;
	}
}
