package entidades;

public class Estado {
	private String idEstado;
	private String estado;
	private String tablaEstado;

	public Estado() {
		super();
	}

	public String getIdEstado() {
		return idEstado;
	}

	public void setIdEstado(String idEstado) {
		this.idEstado = idEstado;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getTablaEstado() {
		return tablaEstado;
	}

	public void setTablaEstado(String tablaEstado) {
		this.tablaEstado = tablaEstado;
	}
}
