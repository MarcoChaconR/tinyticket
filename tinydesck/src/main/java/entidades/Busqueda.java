package entidades;

import java.sql.ResultSet;
import java.util.Vector;

import entidades.Enumeradores.tipoBusqueda;

public class Busqueda {
	private String criterioBusqueda;
	private String llaveBusqueda;
	private tipoBusqueda tipoBusqueda;
	private String[] columnas;
	private Vector<Integer> vectorId;
	private ResultSet setResultados;
	private static Busqueda instancia;

	private Busqueda() {
		super();
	}

	public static Busqueda getInstancia() {
		if (instancia == null) {
			instancia = new Busqueda();
		}
		return instancia;
	}

	public String getCriterioBusqueda() {
		return criterioBusqueda;
	}

	public void setCriterioBusqueda(String criterioBusqueda) {
		this.criterioBusqueda = criterioBusqueda;
	}

	public tipoBusqueda getTipoBusqueda() {
		return tipoBusqueda;
	}

	public void setTipoBusqueda(tipoBusqueda tipoBusqueda) {
		this.tipoBusqueda = tipoBusqueda;
	}

	public String[] getColumnas() {
		return columnas;
	}

	public void setColumnas(String[] columnas) {
		this.columnas = columnas;
	}

	public Vector<Integer> getVectorId() {
		return vectorId;
	}

	public void setVectorId(Vector<Integer> vectorId) {
		this.vectorId = vectorId;
	}

	public ResultSet getSetResultados() {
		return setResultados;
	}

	public void setSetResultados(ResultSet setResultados) {
		this.setResultados = setResultados;
	}

	public static void setInstancia(Busqueda instancia) {
		Busqueda.instancia = instancia;
	}

	public String getLlaveBusqueda() {
		return llaveBusqueda;
	}

	public void setLlaveBusqueda(String cllaveBusqueda) {
		this.llaveBusqueda = cllaveBusqueda;
	}

}
