package entidades;

public class Producto {
	private int idProducto;
	private String categoria;
	private String nombreProducto;
	
	public Producto() {
		super();
	}

	public Producto(int idProducto, String categoria, String nombreProducto) {
		super();
		this.idProducto = idProducto;
		this.categoria = categoria;
		this.nombreProducto = nombreProducto;
	}

	public int getIdProducto() {
		return idProducto;
	}

	public void setIdProducto(int idProducto) {
		this.idProducto = idProducto;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public String getNombreProducto() {
		return nombreProducto;
	}

	public void setNombreProducto(String nombreProducto) {
		this.nombreProducto = nombreProducto;
	}

}
