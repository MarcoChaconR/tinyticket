package reportes;

import com.itextpdf.text.Document;

public interface BuilderPDF {
	public Document construirDocumento();

	public void generarDocumento();
	
	public String getRuta();
}
