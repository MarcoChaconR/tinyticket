package dataAccess;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.Vector;

import javax.swing.JOptionPane;

public class AccesoDatos {
	private final String url;
	private final String user;
	private final String password;

	private Connection connection;

	public AccesoDatos() {
		this.url = "jdbc:mysql://sql732.main-hosting.eu:3306/?useSSL=false";
		this.user = "u902958937_admin";
		this.password = "123Queso*";
	}

	public void conectar() throws SQLException {
		try {
			connection = DriverManager.getConnection(url, user, password);
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "No se obtuvo respuesta de la base de datos", "Error",
					JOptionPane.ERROR_MESSAGE);
		}

	}

	public void desconectar() throws SQLException {
		connection.close();
	}

	// ejecuta select
	public ResultSet ejecutarConsultaParametros(String consulta, Object... parametros) throws SQLException {
		PreparedStatement statement = connection.prepareStatement(consulta);
		for (int i = 0; i < parametros.length; i++) {
			statement.setObject(i + 1, parametros[i]);
		}
		return statement.executeQuery();
	}

	// ejecuta select
	public ResultSet ejecutarConsulta(String consulta) throws SQLException {
		Statement statement = connection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
		return statement.executeQuery(consulta);
	}

	// ejecuta insert, update, delete
	public int ejecutarInsercion(String consulta, Object... parametros) throws SQLException {
		PreparedStatement statement = connection.prepareStatement(consulta);
		for (int i = 0; i < parametros.length; i++) {
			statement.setObject(i + 1, parametros[i]);
		}
		return statement.executeUpdate();
	}

	public int ejecutarActualizacion(String consulta, String[] parametros) throws SQLException {
		PreparedStatement statement = connection.prepareStatement(consulta);
		int i = 1;
		for (Object parametro : parametros) {
			statement.setObject(i, parametro);
			i++;
		}
		return statement.executeUpdate();
	}

	// ejecutar storedprocedures
	public ResultSet ejecutarStoredProcedure(String nombreSP, Vector<Object> parametros) throws SQLException {
		CallableStatement statement = connection.prepareCall("{call " + nombreSP + "}");
		int i = 1;
		for (Object parametro : parametros) {
			statement.setObject(i, parametro);
			i++;
		}
		statement.execute();
		return statement.getResultSet();
	}

	public String consumirStoredProcedureAsignar(String tipoUsuario) throws SQLException {
		CallableStatement statement = connection
				.prepareCall("{call u902958937_TikectDesk.sp_obtenerSiguienteUsuario(?, ?)}");

		statement.setString(1, tipoUsuario);
		statement.registerOutParameter(2, Types.VARCHAR);

		statement.execute();

		String siguienteUsuario = statement.getString(2);

		statement.close();

		return siguienteUsuario;
	}
}
